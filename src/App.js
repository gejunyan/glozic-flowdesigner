import React from 'react'
import './App.css';
import FlowyComp from "../src/lib/index"


function App() {
  const [dis, setDroppable] = React.useState(true)
  const [Nodes, setNodes] = React.useState([{ "id": "node-start", "text": "Start", "data": { "nodeType": "start" }, "height": 39, "width": 200, "nodeType": "start", "icon": "play-circle" }, { "id": "node-stop", "text": "Stop", "data": { "nodeType": "stop" }, "height": 39, "width": 200, "nodeType": "stop", "icon": "stop-circle-o" }, { "id": "node-7cdd5", "text": "True", "nodeType": "conditionTrue", "height": 25, "width": 170 }, { "id": "node-e9eb7", "text": "False", "nodeType": "conditionFalse", "height": 25, "width": 170 }, { "id": "node-a6e44", "text": "Condition", "height": 39, "width": 200, "nodeType": "Condition", "data": { "nodeType": "Condition" }, "icon": "question" }, { "id": "node-bf8d8", "text": "Send Email", "nodeType": "Send Email", "data": { "nodeType": "Send Email" }, "height": 39, "width": 200, "icon": "envelope-o" }, { "id": "node-2d54a", "text": "Send Email", "nodeType": "Send Email", "data": { "nodeType": "Send Email" }, "height": 39, "width": 200, "icon": "envelope-o" }, { "id": "node-1f8b9", "text": "Send SMS", "nodeType": "Send SMS", "data": { "nodeType": "Send SMS" }, "height": 39, "width": 200, "icon": "comments-o" }])
  const [Edges, setEdges] = React.useState([{ "id": "node-start-node-2d54a", "from": "node-start", "to": "node-2d54a" }, { "id": "node-2d54a-node-a6e44", "from": "node-2d54a", "to": "node-a6e44" }, { "id": "node-if-up-af374", "from": "node-a6e44", "to": "node-e9eb7", "arrow": "false", "index": 1 }, { "id": "node-if-up-25a77", "from": "node-a6e44", "to": "node-7cdd5", "arrow": "false", "index": 2 }, { "id": "node-7cdd5-node-1f8b9", "from": "node-7cdd5", "to": "node-1f8b9" }, { "id": "node-1f8b9-node-stop", "from": "node-1f8b9", "to": "node-stop" }, { "id": "node-e9eb7-node-bf8d8", "from": "node-e9eb7", "to": "node-bf8d8" }, { "id": "node-bf8d8-node-stop", "from": "node-bf8d8", "to": "node-stop" }])
  const [Branchs, setBranchs] = React.useState([{ "branch_id": "node-a6e44", "stop_node_id": "node-stop" }])

  const [isShow, setIsShow] = React.useState(true);

  const onNodeClick = (event, node) => {
    console.log("On onNodeClick");
    console.log(event, node);
  };

  const onChange = (data) => {
    console.log(JSON.stringify(data));
    setNodes(data.nodes);
    setEdges(data.edges);
    setBranchs(data.branchs);
  };
  const onSave = (data) => {
  }

  const handleToggle = () => {
    setIsShow(!isShow);
  };

  return (
    <>
      {/* <button style={{ zIndex: "99999999999", position: "absolute" }} onClick={() => setDroppable(true)}>on</button>
      <button style={{ zIndex: "99999999999", position: "absolute", left: "50px" }} onClick={() => setDroppable(false)}>off</button> */}

      {dis &&
        <FlowyComp
          isToggele={true}
          isShow={isShow}
          handleToggle={handleToggle}
          buttonRef={"buttonRef"}
          onNodeClick={onNodeClick}
          onChange={onChange}
          onDragEnd={onSave}
          onCancel={onSave}
          onSave={onSave}
          Edges={Edges}
          Nodes={Nodes}
          branchs={Branchs}
          options={{
            actionLists: [
              {
                groupName: 'Function',
                actions: [
                  { logMessage: true },
                  { jseditor: true },
                  { toast: true },
                  { redirect: true },

                ]
              },
              {
                groupName: 'Service',
                actions: [
                  { sendEmail: true },
                  { sendSms: true },
                ]
              },
              {
                groupName: 'Logic and  control',
                actions: [
                  { loop: true },
                  { condition: true },
                  { branchCondition: true }
                ]
              },
              {
                groupName: 'Integration',
                actions: [
                  { qrcode: true },
                  { getSharepointlist: true },
                  { setFormComponent: true },
                  { collectionOperation: true },
                  { callWebService: true },
                  { queryJson: true },
                  { renderwebcontent: true }
                ]
              }
            ]
            // actions: [
            //   { callWebService: true },
            //   { queryJson: true },
            //   { toast: true },
            //   { condition: true },
            //   { sendEmail: true },
            //   { collectionOperation: true },
            //   { redirect: true },
            //   { loop: true },
            // ]
          }} />
      }
    </>)
}

export default App;


