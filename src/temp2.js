import React, { useEffect, useState, useRef } from 'react';
import { Canvas, hasLink, Node, createEdgeFromNodes, addNodeAndEdge, useSelection, Edge, removeAndUpsertNodes, Port, Add, upsertNode, Icon, Remove, Label, CanvasRef } from 'reaflow';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import CustomToggle from './CustomToggle';
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import { useDragControls, motion } from 'framer-motion'
import ActionCard from './ActionCard';
import nodeconfig from './nodeconfig'
import './index.css';

let enteredEdge = null;
let enteredNode = null;
let isCondition = null;
let nodeIndex = "";
let activeDrag = "";
let perentChild = "";
let nodeIcon = "";
const FlowCompo = (props) => {
  console.log(props.Nodes)
  const [droppable, setDroppable] = useState(false);
  const [nodes, setNodes] = useState((props.Nodes && props.Nodes.length != 0) ? props.Nodes : nodeconfig.defaultNodes);
  const [edges, setEdges] = useState((props.Edges && props.Edges.length != 0) ? props.Edges : nodeconfig.defaultEdge);
  const [selectNode, setSelectNode] = useState(false);
  const [hiddenEdges, setHiddenEdges] = useState([]);
  const [stopArr, setStopArr] = useState([]);
  const [actionconfig, setActionconfig] = useState(props.options.actions || []);
  const dragControls = useDragControls();
  const [zoom, setZoom] = useState(0.7);
  const ref = useRef(null);
  const { selections, onCanvasClick, onClick, onKeyDown, clearSelections } = useSelection({
    nodes,
    edges,
    selections: [],
    onDataChange: (n, e) => {
      console.info('Data changed', n, e);
      setNodes(n);
      setEdges(e);
    },
    onSelection: (s) => {
      console.info('Selection', s);
      console.log(edges);
    },
  });

  useEffect(() => {
    if (props.options && props.options.actions) {
      setActionconfig(props.options.actions);
    } if ((props.Nodes && props.Nodes.length != 0)) {
      setNodes(props.Nodes);
    } if ((props.Edges && props.Edges.length != 0)) {
      setEdges(props.Edges);
    }
  }, [props]);

  const onDragStart = (event, title, index, nodeIcons) => {
    console.log('Start of Dragging', title);
    console.log("nodeIcon", nodeIcon);
    if (title === 'Condition') {
      isCondition = true;
    } else {
      isCondition = false;
    }
    activeDrag = title;
    nodeIndex = index;
    nodeIcon = nodeIcons;

    setDroppable(true)
    dragControls.start(event, { snapToCursor: true });
  };

  const onDragEnd = (event, data) => {
    console.log("event drand end ", event);
    console.log("event drand end ", data);
    debugger
    const id = `node-${nodeconfig.uuidv4()}`;
    let results = null;
    if (droppable && isCondition === false && (enteredEdge || enteredNode)) {
      const newNode = {
        id,
        text: activeDrag,
        nodeType: activeDrag,
        data: {
          nodeType: activeDrag
        },
        height: 50,
        width: 170,
        icon: {
          url: nodeIcon,
          height: 25,
          width: 25
        }
      };

      results = upsertNode(nodes, edges, enteredEdge, newNode);
      if (results) {
        const singleNodes2 = stopArr.filter(function (o1) {
          return !results.edges.some(function (o2) {
            return o1.from == o2.from;
          });
        });

        setNodes(results.nodes);
        setEdges(results.edges.concat(singleNodes2));
        props.onChange({ nodes: results.nodes, edges: results.edges.concat(singleNodes2) });
        props.onDragEnd(newNode)

      }
      perentChild = "";
    } else if (droppable && isCondition === true && (enteredEdge || enteredNode)) {

      const id1 = `node-${nodeconfig.uuidv4()}`;
      const id2 = `node-${nodeconfig.uuidv4()}`;
      const id3 = `node-${nodeconfig.uuidv4()}`;
      const conditionNdes = [
        {
          id: id3,
          text: 'False',
          height: 50,
          width: 170,
        },
        {
          id: id2,
          text: 'True',
          height: 50,
          width: 170,
        },

      ]
      const conditionEdges = [
        {
          id: `node-${nodeconfig.uuidv4()}`,
          from: `${id1}`,
          to: `${id2}`
        },
        {
          id: `node-${nodeconfig.uuidv4()}`,
          from: `${id1}`,
          to: `${id3}`
        }
      ];

      const condition = {
        id: id1,
        text: 'If',
        height: 80,
        width: 75,
        nodeType: activeDrag,
        data: {
          nodeType: activeDrag
        },
        icon: {
          url: nodeIcon,
          height: 25,
          width: 25
        }
      }
      const results = upsertNode([...nodes, ...conditionNdes], [...edges, ...conditionEdges], enteredEdge, condition);

      setHiddenEdges(old => [...old, id1])
      setNodes(results.nodes);

      let getEndedge = results.edges.find(x => x.to == enteredEdge.to);
      let newEdges = results.edges.filter(x => x.to != enteredEdge.to);
      const nConnect = [
        {
          id: `node-${nodeconfig.uuidv4()}`,
          from: `${id3}`,
          to: `${getEndedge.to}`
        },
        {
          id: `node-${nodeconfig.uuidv4()}`,
          from: `${id2}`,
          to: `${getEndedge.to}`
        }
      ]
      setStopArr(old => [...old, ...nConnect])
      newEdges = newEdges.concat(nConnect)

      const singleNodes = stopArr.filter(function (o1) {
        return !newEdges.some(function (o2) {
          return o1.from == o2.from;
        });
      });

      newEdges = newEdges.concat(singleNodes)
      setEdges(old => [...newEdges]);
      props.onChange({ nodes: results.nodes, edges: [...newEdges] });
    }

    activeDrag = "";
    enteredEdge = null;
    setDroppable(false);

  };

  const edgeList = (edges) => {
    let uniqueChars = [];
    edges.forEach((c) => {
      if (!uniqueChars.includes(c)) {
        uniqueChars.push(c);
      }
    });
    return uniqueChars;
  }

  const removeNodes = (nodes, edge, arr, node, dummy) => {
    const result = removeAndUpsertNodes(nodes, edge, node);
    setEdges(result.edges);
    setNodes(result.nodes);
    props.onChange({ nodes: result.edges, edges: result.nodes });
    clearSelections();
    arr.splice(0, 1);
    if (node.text !== 'If') {
      const findNodeEdge = dummy.find(x => x.from === node.id)
      const findEdgeIndex = dummy.indexOf(findNodeEdge)
      dummy.splice(findEdgeIndex, 1);
      const findChild = edge.find(x => x.from === node.id)
    } else {
      const findParent = edge.find(x => x.to === node.id)
      const findChild = edge.find(x => x.from === findParent.to)
      const childOfChild = edge.find(x => x.from === findChild.to)
      perentChild = `${findParent.from}-${childOfChild.to}`;
    }
    if (arr.length !== 0) {
      removeNodes(result.nodes, result.edges, arr, arr[0], dummy);
    } else {
      const ids = result.edges.map(o => o.id);
      const filtered = result.edges.filter(({ id }, index) => !ids.includes(id, index + 1));
      setEdges(filtered);
      props.onChange({ nodes: nodes, edges: filtered });
      setStopArr(dummy);
    }
  }

  return (
    <div style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }}>
      <div className="react-flow-left">

        {actionconfig.some(x => x.callWebService) && <motion.div className="block" onMouseDown={event => onDragStart(event, 'Call Web Service', 0, 'https://constantsys.com/buket/plug.svg')}><ActionCard cardName='Call Web Service' icon='plug' holderIndex={-1} /></motion.div>}
        {actionconfig.some(x => x.queryJson) && <motion.div className="block" onMouseDown={event => onDragStart(event, 'Query Json', 1, 'https://constantsys.com/buket/filter.svg')}><ActionCard cardName='Query Json' icon='filter' holderIndex={-1} /></motion.div>}
        {actionconfig.some(x => x.toast) && <motion.div className="block" onMouseDown={event => onDragStart(event, 'Toast', 2, 'https://constantsys.com/buket/toast.svg')}><ActionCard cardName='Toast' icon='bell-o' holderIndex={-1} /></motion.div>}
        {actionconfig.some(x => x.ifCondition) && <motion.div className="block" onMouseDown={event => onDragStart(event, 'Condition', 3, 'https://constantsys.com/buket/condition.svg')}><ActionCard cardName='Condition' icon='question' holderIndex={-1} /></motion.div>}
        {actionconfig.some(x => x.sendEmail) && <motion.div className="block" onMouseDown={event => onDragStart(event, 'Send Email', 4, 'https://constantsys.com/buket/envelope.svg')}><ActionCard cardName='Send Email' icon='envelope-o' holderIndex={-1} /></motion.div>}
        {!actionconfig.some(x => x.sendSMS) && <motion.div className="block" onMouseDown={event => onDragStart(event, 'Send Email', 5, 'https://constantsys.com/buket/sms.svg')}><ActionCard cardName='Send SMS' icon='comments-o' holderIndex={-1} /></motion.div>}
        {!actionconfig.some(x => x.collectionOperation) && <motion.div className="block" onMouseDown={event => onDragStart(event, 'Send Email', 6, 'https://constantsys.com/buket/database.svg')}><ActionCard cardName='Collection Operation' icon='database' holderIndex={-1} /></motion.div>}
        {!actionconfig.some(x => x.redirect) && <motion.div className="block" onMouseDown={event => onDragStart(event, 'Send Email', 7, 'https://constantsys.com/buket/redirect.svg')}><ActionCard cardName='Redirect' icon='share-square' holderIndex={-1} /></motion.div>}
        {!actionconfig.some(x => x.redirect) && <motion.div className="block" onMouseDown={event => onDragStart(event, 'Send Email', 8, 'https://constantsys.com/buket/retweet.svg')}><ActionCard cardName='While Loop' icon='retweet ' holderIndex={-1} /></motion.div>}
        {/* {actionconfig.some(x => x.redirect) && <motion.div className="block" onMouseDown={event => onDragStart(event, 'Send Email', 4)}><ActionCard cardName='Branch' icon='retweet ' holderIndex={-1} /></motion.div>} */}

        <div className='d-flex justify-content-center workflow-buttton'>
          <button className='custom-btn btn btn-flow-canceled' onClick={props.onCancel}>Cancel</button>
          <button className='custom-btn ml-2 btn btn-flow-save' onClick={props.onSave}>Save</button>
        </div>
      </div>
      <div className="react-flow-right">
        <pre style={{ zIndex: 9, position: 'absolute', bottom: 15, right: 15, background: 'rgba(0, 0, 0, .5)', padding: 20, color: 'white' }}>
          Zoom: {zoom}<br />
          <button style={{ display: 'block', width: '100%', margin: '5px 0' }} onClick={() => ref.current.zoomIn()}>Zoom In</button>
          <button style={{ display: 'block', width: '100%', margin: '5px 0' }} onClick={() => ref.current.zoomOut()}>Zoom Out</button>
          <button style={{ display: 'block', width: '100%' }} onClick={() => ref.current.fitCanvas()}>Fit</button>
        </pre>

        <Canvas
          ref={ref}
          nodes={nodes}
          edges={edgeList(edges)}
          selections={selections}
          onEnter={(event, edge) => { console.log(edge); enteredEdge = edge }}
          onLayoutChange={layout => console.log('Layout', layout)}
          onNodeLink={(event, edge) => { console.log("onNodeLink", edge); }}
          node={(event) => {

            let nodeclass = 'defaultNodeStyle', textFill = "#000"
            if (event.properties.data?.nodeType === 'start') {
              nodeclass = 'startNodeStyle';
              textFill = "#fff"
            } else if (event.properties.data?.nodeType === 'stop') {
              nodeclass = 'stopNodeStyle';
              textFill = "#fff"
            } else if (selectNode && selectNode.id == event.properties.id) {
              nodeclass = 'selectNodeStyle';
              textFill = "#fff"
            }

            if (event.properties.data?.nodeType != "while") {
              return (
                <foreignObject
                  onClick={() => {
                    console.log(event.properties);
                    setSelectNode(event.properties)
                    props.onNodeClick(event.properties);
                  }}
                  height={event.properties.height} width={event.properties.width} x={0} y={0}>
                  {event.properties.nodeType != "Condition" &&
                    <div className={`${nodeclass} node-child`}>
                      {event.properties.icon && <img src={event.properties.icon.url} height={event.properties.icon.height} width={event.properties.icon.width} />}
                      <label style={{ color: textFill }}>{event.properties.text}</label>
                    </div>
                  }

                  {event.properties.nodeType == "Condition" &&
                    <div className={`${nodeclass} node-child consdition-node`} style={{ padding: "0px", height: "79px" }}>
                      {event.properties.icon && <img style={{ marginRight: "0px" }} src={event.properties.icon.url} height={75} width={75} />}
                      <label style={{ color: textFill, position: 'absolute', top: "17%" }}>{event.properties.text}</label>
                    </div>
                  }

                </foreignObject>
              )
            } else {
              return (
                <Node
                  width={"250"}
                  rx={0}
                  ry={0}
                  className={`${nodeclass} flow-nodes`}
                  dragCursor="grab"
                  dragType="node"
                  label={<Label className='flow-nodes-label' style={{ fill: textFill }} />}
                  icon={<Icon className='flow-nodes-icon' />}
                  removable={false}
                  port={
                    <Port
                      className='flow-nodes-port'
                      onClick={(e, node) => {
                        console.log('onClick port: ', node);
                      }}
                      onEnter={(e, node) => {
                        console.log('onEnter port: ', node);
                      }}
                      onLeave={(e, node) => {
                        console.log('onLeave port: ', node);
                      }}
                    />
                  }
                  onClick={(event, node) => {
                    console.log('Selecting Node', event, node);
                    console.log(selections);
                    setSelectNode({ event, node })
                    onClick(event, node);
                    props.onNodeClick(event, node);
                  }}
                  onKeyDown={(event, node) => {
                    console.log('Keydown Event', node, event);
                    onKeyDown(event);
                  }}
                  onRemove={(event, node) => {


                    // if (node.text === 'If') {
                    //   const ed = edges.filter((x) => {
                    //     return x.from === node.id
                    //   });
                    //   const result2 = nodes.filter(function (o1) {
                    //     return ed.some(function (o2) {    //  for diffrent we use NOT (!) befor obj2 here
                    //       return o1.id == o2.to;          // id is unnique both array object
                    //     });
                    //   });
                    //   result2.splice(0, 0, node);
                    //   // result2.push(node)
                    //   removeNodes(nodes, edges, result2, result2[0], stopArr);
                    // } else if (node.text !== 'True' && node.text !== 'False') {
                    //   const result = removeAndUpsertNodes(nodes, edges, node);
                    //   console.log(result);
                    //   console.log("remove", node);
                    //   setEdges(result.edges);
                    //   setNodes(result.nodes);
                    //   props.onChange({ nodes: result.nodes, edges: result.edges })
                    //   clearSelections();
                    // }

                  }}
                />
              )
            }


          }}

          onNodeLinkCheck={(_event, from, to) => {
            if (from.id === to.id) {
              console.log("===============1");
              return false;
            }

            if (hasLink(edges, from, to)) {
              console.log("=============2");
              return false;
            }
            console.log("=================3");
            return true;
          }}

          edge={(edge) => (
            <Edge
              {...edge}
              add={<Add hidden={(!droppable || !(droppable && !hiddenEdges.some(x => x == edge.properties.from)))}
                onEnter={(event, edges) => { console.log(edge, hiddenEdges); enteredEdge = edge.properties }}
                onLeave={(event, edges) => { enteredEdge = null }}
              />}
              onEnter={(event, edge) => { console.log(edge); }}
              onClick={(event, node) => {
                console.log('Selecting Node', event, node);
                console.log(selections);
                onClick(event, node);
              }}
              onRemove={(event, edge) => {
                console.log('Removing Edge', event, edge);
                setEdges(edges.filter(e => e.id !== edge.id));
                clearSelections();
              }}
            />
          )}

          onEnter={(event, edge) => { console.log(edge); }}

          onCanvasClick={(event) => {
            console.log('Canvas Clicked', event);
            onCanvasClick();
          }}

          onNodeLink={(_event, from, to) => {
            const newEdges = edges.filter(e => e.to !== from.id);

            setEdges([
              ...newEdges,
              createEdgeFromNodes(to, from)
            ]);
          }}

          onZoomChange={z => {
            console.log('zooming', z);
            setZoom(z);
          }}
        />

      </div>
      <div>
        <motion.div
          drag
          dragControls={dragControls}
          className="dragger"
          onDragEnd={onDragEnd}
        >
          {activeDrag && (
            <div className="dragInner">
              {activeDrag}
            </div>
          )}
        </motion.div>
      </div>
    </div>
  );
}


export default FlowCompo;


