import React from 'react'
import './ActionCard.css'

const style = {
  border: 'rounded 1px dashed gray',
  marginBottom: '0.2rem',
  backgroundColor: 'white',
  cursor: 'pointer',
  float: 'left',
  width: '100%',
  maxWidth: '200px',
  minWidth: '200px',
  verticalAlign: 'middle'
}

const nodeStyle = {
  border: 'rounded 1px dashed gray',
  marginBottom: '0.2rem',
  backgroundColor: 'white',
  cursor: 'pointer',
  float: 'left',
  width: '100%',
  verticalAlign: 'middle'
}

const style1 = {
  border: '1px dashed #2bbbad',
  backgroundColor: '#2bbbad',
  color: "#fff",
  cursor: 'pointer',
  float: 'left',
  width: '37px',
  margin: '0 auto',
  height: '38px'
}

const style2 = {
  backgroundColor: '#fff  ',
  cursor: 'pointer',
  float: 'left',
  width: '100%',
  height: '37px',
  verticalAlign: 'middle',
  color: "#000"
}

function ActionCard({ icon, cardName, holderIndex, isDrage }) {
  let actionCardStyle = JSON.parse(JSON.stringify(style))
  let actionCardStyle1 = JSON.parse(JSON.stringify(style1))
  let actionCardStyle2 = JSON.parse(JSON.stringify(style2))
  actionCardStyle.backgroundColor = isDrage ? "rounded 1px dashed gray" : style.backgroundColor
  actionCardStyle1.backgroundColor = isDrage ? "rgb(43 187 173 / 54%)" : style1.backgroundColor
  actionCardStyle2.backgroundColor = isDrage ? "rgb(255 255 255 / 25%)" : style2.backgroundColor
  return (
    <div className="d-flex justify-content-start .z-depth-1-half" style={{ ...actionCardStyle }}>
      <div className="p-2 col-example text-left"
        style={{ ...actionCardStyle1 }}><i className={"fa fa-" + icon} aria-hidden="true"></i></div>
      <div className="py-2 pl-1 col-example text-left" style={{ ...actionCardStyle2 }}>{cardName}</div>
    </div>
  )
}

function NodeActionCard({ icon, cardName, holderIndex, event, props, droppable, masterNode, onClick, onMouseDown }) {
  return (
    <div className="d-flex justify-content-start .z-depth-1-half" style={{ ...nodeStyle }}>
      <div className="p-2 col-example text-left"
        onClick={onClick}
        onMouseDown={onMouseDown}
        style={{ ...style1 }}><i className={"fa fa-" + icon} aria-hidden="true"></i></div>
      <div className="py-2 pl-1 col-example text-left" style={{ ...style2 }}>{cardName}</div>
      {masterNode &&
        <i
          className="fa fa-cog position-absolute"
          style={{ top: "10px", right: "9px" }}
          aria-hidden="true"
          onMouseOver={(ey) => {
            if (!droppable) {
              props.setSelectNode(event.properties)
              props.setNodeSelected({ x: ey.clientX - 2, y: ey.clientY - 10 })
              props.setSelectMore(true)
            }
          }}></i>
      }
    </div>
  )
}

export { ActionCard, NodeActionCard }