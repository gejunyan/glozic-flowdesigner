const uuidv4 = () => {
    return 'xxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const startNodeId = `node-start`;
const stopNodeId = `node-stop`;
const defaultNodes = [
    {
        id: `${startNodeId}`,
        text: 'Start',
        data: {
            nodeType: 'start'
        },
        height: 39,
        width: 200,
        nodeType: 'start',
        icon: "play-circle"
    },
    {
        id: `${stopNodeId}`,
        text: 'Stop',
        data: {
            nodeType: 'stop'
        },
        height: 39,
        width: 200,
        nodeType: 'stop',
        icon: "stop-circle-o"
    }
]

const defaultEdge = [
    {
        id: `edge-${uuidv4()}`,
        from: `${startNodeId}`,
        to: `${stopNodeId}`
    }
]


export default {
    defaultNodes,
    defaultEdge,
    uuidv4
}