import React, { useEffect, useState } from 'react'
import { Col, Card, CardHeader, Collapse, CardBody } from 'reactstrap';
import { useDragControls, motion } from 'framer-motion'
import { ActionCard } from './ActionCard';


function ReactFlowLeft(props) {
    const [search, setSearch] = useState("");
    const [indexId, setIndexId] = useState(null);
    const [actionconfig, setActionconfig] = useState(props.options.actionLists || []);

    const actionLists = [{
        'type': 'callWebService',
        "title": 'Call Web Service',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Call Web Service', 0, 'plug')}><ActionCard cardName='Call Web Service' icon='plug' holderIndex={-1} /></motion.div>
    }, {
        'type': 'queryJson',
        "title": 'Query Json',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Query Json', 1, 'filter')}><ActionCard cardName='Query Json' icon='filter' holderIndex={-1} /></motion.div>
    }, {
        'type': 'toast',
        "title": 'Toast',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Toast', 2, 'bell-o')}><ActionCard cardName='Toast' icon='bell-o' holderIndex={-1} /></motion.div>
    }, {
        'type': 'condition',
        "title": 'Condition',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Condition', 3, 'question')}><ActionCard cardName='Condition' icon='question' holderIndex={-1} /></motion.div>
    }, {
        'type': 'sendEmail',
        "title": 'Send Email',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Send Email', 4, 'envelope-o')}><ActionCard cardName='Send Email' icon='envelope-o' holderIndex={-1} /></motion.div>
    }, {
        'type': 'sendSms',
        "title": 'Send Email',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Send SMS', 5, 'comments-o')}><ActionCard cardName='Send SMS' icon='comments-o' holderIndex={-1} /></motion.div>
    }, {
        'type': 'collectionOperation',
        "title": 'Collection Operation',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Collection', 6, 'database')}><ActionCard cardName='Collection Operation' icon='database' holderIndex={-1} /></motion.div>
    }, {
        'type': 'redirect',
        "title": 'Redirect',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Redirect', 7, 'share-square')}><ActionCard cardName='Redirect' icon='share-square' holderIndex={-1} /></motion.div>
    }, {
        'type': 'loop',
        "title": 'Loop',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Loop', 8, 'Loop')}><ActionCard cardName='Loop' icon='retweet ' holderIndex={-1} /></motion.div>
    },
    {
        'type': 'branchCondition',
        "title": 'Branch',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Branch', 9, 'question')}><ActionCard cardName='Branch' icon='question' holderIndex={-1} /></motion.div>
    },
    {
        'type': 'getSharepointlist',
        "title": 'Get Sharepoint list',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Get Sharepoint list', 10, 'question')}><ActionCard cardName='Get Sharepoint List' icon='question' holderIndex={-1} /></motion.div>
    },
    {
        'type': 'setFormComponent',
        "title": 'Set Form Component',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Set Form Component', 11, 'question')}><ActionCard cardName='Set Form Component' icon='question' holderIndex={-1} /></motion.div>
    },
    {
        'type': 'renderwebcontent',
        "title": 'renderwebcontent',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Render Content', 11, 'futbol-o')}><ActionCard cardName='Render Web content' icon='futbol-o' holderIndex={-1} /></motion.div>
    }, {
        'type': 'jseditor',
        "title": 'jseditor',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'JS Editor', 12, 'futbol-o')}><ActionCard cardName='JS Editor' icon='futbol-o' holderIndex={-1} /></motion.div>
    },
    {
        'type': 'logMessage',
        "title": 'logMessage',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Log Message', 12, 'history')}><ActionCard cardName='Log Message' icon='history' holderIndex={-1} /></motion.div>
    },
    {
        'type': 'startWorkflow',
        "title": 'startWorkflow',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'Start Workflow', 12, 'play-circle')}><ActionCard cardName='Start Workflow' icon='play-circle' holderIndex={-1} /></motion.div>
    },{
        'type': 'qrcode',
        "title": 'QR code',
        "nodes": <motion.div className="block" onClick={props.onDragEnd} onMouseDown={event => props.onDragStart(event, 'QR code', 13, 'qrcode')}><ActionCard cardName='QR code' icon='qrcode' holderIndex={-1} /></motion.div>
    }];

    useEffect(() => {
        if (props.options && props.options.actionLists) {
            setActionconfig(props.options.actionLists);
        }
    }, [props]);


    return (
        <div className="react-flow-left overflow-hidden" >
            <div className='scrollbar' style={{ height: '90vh', overflow: 'overlay' }}>
                <div id="task-palette-search-wrapper"><div className="topnav-search">
                    <div className="search-container d-flex"><button type="submit">
                        <i className="fa fa-search"></i></button>
                        <input value={search} onChange={(e) => setSearch(e.target.value)} type="text" placeholder="Search.." name="search" />
                    </div>
                </div>
                </div>
                {search == "" &&
                    <>
                        {actionconfig.map((item, index) =>
                            <Card key={index} className="text-left  border Auth-card shadow-card rounded-lg mb-3" >
                                <CardHeader className="p-0 border-bottom Auth-card-header rounded-top" onClick={() => setIndexId((index == indexId ? -1 : index))}>
                                    <div className="align-items-center d-flex justify-content-between " style={{ color: " #9e9e9e" }}>
                                        <span style={{ "paddingLeft": "10px", "paddingTop": "10px", "fontSize": "15px", "paddingBottom": "10px" }}><strong>{item.groupName}</strong></span>
                                        <div className="d-flex">
                                            <span aria-expanded={index == indexId} className='mr-2 pointer'>
                                                {index == indexId && <i className="fa fa-angle-up fa-2x" aria-hidden="true"></i>}
                                                {index != indexId && <i className="fa fa-angle-down fa-2x" aria-hidden="true"></i>}
                                            </span>
                                        </div>
                                    </div>
                                </CardHeader>
                                <Collapse isOpen={index == indexId} style={{ 'width': '100%' }}>
                                    <div style={{ background: "#7f7f7f4a" }}>
                                        {actionLists.filter(z => item.actions.map(x => Object.keys(x)[0]).includes(z.type)).map((x, indexY) => <div key={indexY}>{x.nodes}</div>)}
                                    </div>
                                </Collapse>
                            </Card>
                        )}
                    </>
                }

                {search != "" &&
                    <>
                        {actionLists.filter(z => z.title.toLocaleLowerCase().search(search) >= 0).map(x => x.nodes)}
                    </>
                }

            </div>

            <div className='d-flex justify-content-center workflow-buttton'>
                <button className={`custom-btn btn btn btn-flow-save ${props.buttonRef ? 'd-none' : ''}`} onClick={props.onCancel} >Close</button>
                <button className={`custom-btn ml-2 btn btn-flow-save ${props.buttonRef ? 'd-none' : ''}`} onClick={props.onSave} id={props.buttonRef} name={props.buttonRef} >Save</button>
            </div>
        </div>
    )
}

export default ReactFlowLeft