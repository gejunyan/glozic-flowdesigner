const uuidv4 = () => {
    return 'xxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const startNodeId = `node-start`;
const stopNodeId = `node-stop`;
const defaultNodes = [
    {
        id: '1',
        text: '1',
        data: {
            nodeType: 'start'
        },
        height: 50,
        width: 170,
        icon: {
            url: 'https://constantsys.com/buket/play-svgrepo-com.svg',
            height: 25,
            width: 25,
            let: 15
        }
    },
    {
        id: '2',
        data: {
            nodeType: 'while'
        },
    },
    {
        id: '2-1-1',
        text: '2 > 1.1',
        parent: '2',
        icon: {
            url: 'https://constantsys.com/buket/filter.svg',
            height: 25,
            width: 25,
            let: 15
        }
    },
    {
        id: '2-1-2',
        text: '2 > 1.2',
        parent: '2',
        icon: {
            url: 'https://constantsys.com/buket/filter.svg',
            height: 25,
            width: 25,
            let: 15
        }
    },
    {
        id: '2-1-3',
        text: '2 > 1.3',
        parent: '2',
        icon: {
            url: 'https://constantsys.com/buket/filter.svg',
            height: 25,
            width: 25,
            let: 15
        }
    },
    {
        id: '3',
        text: '3',
        data: {
            nodeType: 'stop'
        },
        height: 50,
        width: 170,
        icon: {
            url: 'https://constantsys.com/buket/filter.svg',
            height: 25,
            width: 25
        }
    }
]

const defaultEdge = [
    {
      id: '1-2',
      from: '1',
      to: '2'
    },
    {
      id: '2-1-1>2-1-2',
      from: '2-1-1',
      to: '2-1-2',
      parent: '2'
    },
    {
      id: '2-1-1>2-1-3',
      from: '2-1-1',
      to: '2-1-3',
      parent: '2'
    },
    {
      id: '2-3',
      from: '2',
      to: '3'
    },
  ]


export default {
    defaultNodes,
    defaultEdge,
    uuidv4
}