import React, { useEffect, useState, useRef } from 'react';
import {
  Canvas,
  hasLink,
  Node,
  createEdgeFromNodes,
  addNodeAndEdge,
  useSelection,
  Edge,
  removeAndUpsertNodes,
  Port,
  Add,
  upsertNode,
  Icon,
  Remove,
  Label,
  CanvasRef,
  useProximity
} from 'reaflow';
import { Modal, } from "reactstrap";
import { useDragControls, motion } from 'framer-motion'
import ReactFlowLeft from './TaskPalette';
import nodeconfig from './nodeconfig';
import { ActionCard, NodeActionCard } from './ActionCard';
import './index.css';
import { debug } from 'dagre';

let enteredEdge = null;
let enteredNode = null;
let nodeIndex = "";
let activeDrag = "";
let perentChild = "";
let nodeIcon = "";
let moveNode = null;
let isNodeCheck = false
const nodeTypeCondition = ['start', 'stop', 'conditionFalse', 'conditionTrue', 'conditionBranch', 'whileStart', 'whileStop']

const FlowCompo = (props) => {
  const [droppable, setDroppable] = useState(false);
  const [nodes, setNodes] = useState((props.Nodes && props.Nodes.length != 0) ? props.Nodes : nodeconfig.defaultNodes);
  const [edges, setEdges] = useState((props.Edges && props.Edges.length != 0) ? props.Edges : nodeconfig.defaultEdge);
  const [branchs, setBranchs] = useState((props.branchs && props.branchs.length != 0) ? props.branchs : []);
  const [selectNode, setSelectNode] = useState(false);
  const [selectMore, setSelectMore] = useState(false);
  const [hiddenEdges, setHiddenEdges] = useState([]);
  const [stopArr, setStopArr] = useState([]);
  const [nodeSelected, setNodeSelected] = useState([]);
  const dragControls = useDragControls();
  const [zoom, setZoom] = useState(0.7);
  const [modal_rename, setModalRename] = useState(false);
  const [renameNode, setRenameNode] = useState("");
  const [selectPast, setSelectPast] = useState(false);
  const ref = useRef(null);
  const { selections, onCanvasClick, onClick, onKeyDown, clearSelections } = useSelection({
    nodes, edges, selections: [], onDataChange: (n, e) => {
      console.info('Data changed', n, e);
      setNodes(n);
      setEdges(e);
    },
    onSelection: (s) => {
      console.info('Selection', s);
      console.log(edges);
    },
  });

  useEffect(() => {
    if ((props.Nodes && props.Nodes.length != 0)) {
      setNodes(props.Nodes);
    } if ((props.Edges && props.Edges.length != 0)) {
      setEdges(props.Edges);
    }
  }, [props]);

  const onDragStart = (event, title, index, nodeIcons) => {
    enteredEdge = "";
    activeDrag = title;
    nodeIndex = index;
    nodeIcon = nodeIcons;
    console.log(nodeIcon);
    setDroppable(true)
    dragControls.start(event, { snapToCursor: true });
  };

  const onMoveDragStart = async (event, node) => {
    setTimeout(() => {
      if (!node.selectionDisabled && !isNodeCheck) {
        console.log('Start of Dragging', node);
        console.log('event', event);
        enteredEdge = "";
        activeDrag = (!node.text || node.text == "") ? node.nodeType : node.text;
        nodeIndex = node.data ? node.data.nodeIndex : null;
        nodeIcon = node.icon
        moveNode = node
        setDroppable(true)
        setSelectMore(false)
        dragControls.start(event, { snapToCursor: true });
      }
    }, 150);
  };

  const onDragEnd = (event, data) => {
    debugger
    isNodeCheck = true;
    setTimeout(() => { isNodeCheck = false; }, 160)
    try {
      const id = `node-${nodeconfig.uuidv4()}`;
      let results = null;
      //Move Nodes
      if (moveNode && enteredEdge) {

        if (!edges.some(x => (x.from == enteredEdge.from && x.to == moveNode.id && moveNode.id == enteredEdge.to) || (x.from == moveNode.id && x.to == enteredEdge.to && moveNode.id == enteredEdge.from))) {
          console.log("moveNode", moveNode)
          console.log("enteredEdge", enteredEdge)
          console.log("edges", edges);

          if (moveNode.data.nodeType == "Condition" || moveNode.nodeType == "Branch Condition") {

            let branch = branchs.find(x => x.branch_id == moveNode.id) ? branchs.find(x => x.branch_id == moveNode.id) : null
            let edgeLinkFrom = edges.find(x => x.to == branch.branch_id)
            let edgeConditionTo = edges.find(x => x.to == branch.stop_node_id);
            let upsetEdges = edges.filter(x => (!(x.to == branch.stop_node_id) && !(x.to == branch.branch_id) && !(x.from == enteredEdge.from)));
            let upsetBranchEdges = edges.filter(x => ((x.to == branch.stop_node_id) || (x.to == branch.branch_id)));

            let upsetbranch = branchs;
            upsetEdges.push({
              from: edgeLinkFrom.from,
              id: `node-${nodeconfig.uuidv4()}`,
              to: edgeConditionTo.to,
              parent: edgeConditionTo.parent
            });

            upsetBranchEdges.forEach(ele => {
              if ((ele.to == branch.branch_id)) {
                ele.from = enteredEdge.from;
                ele["parent"] = edgeConditionTo.parent
              }
              if (ele.to == branch.stop_node_id) {
                ele.to = enteredEdge.to;
                ele["parent"] = edgeConditionTo.parent
              }
              upsetEdges.push(ele)
            });

            upsetbranch.forEach(ele => {
              if (ele.branch_id == moveNode.id) {
                ele.stop_node_id = enteredEdge.to
              }
            });

            setSelectMore(false)
            props.onChange({ nodes: nodes, edges: upsetEdges, branchs: upsetbranch })
            clearSelections();
          } else {
            let edgeLinkfilter = edges.find(x => x.from == moveNode.id)
            let edgeConditionFrom = edges.find(x => x.to == moveNode.id)

            let upsetEdges = [];
            let upsetNodes = nodes;

            let isCheck = 0
            edges.forEach(ele => {
              if (!(ele.id == edgeLinkfilter.id || ele.id == edgeConditionFrom.id)) {
                upsetEdges.push(ele)
              } else if ((ele.id == edgeLinkfilter.id) && (isCheck == 0)) {
                upsetEdges.push({
                  from: edgeConditionFrom.from,
                  id: `node-${nodeconfig.uuidv4()}`,
                  to: edgeLinkfilter.to,
                  parent: enteredEdge.parent
                });
              }
            })


            upsetEdges.forEach(ele => {
              if (enteredEdge.to == ele.to && enteredEdge.from == ele.from) {
                ele.from = moveNode.id;
              }
            });

            upsetNodes.forEach(ele => {
              if (ele.id == moveNode.id) {
                ele.parent = enteredEdge.parent
              }
            })

            upsetEdges.push({
              from: enteredEdge.from,
              id: `node-${nodeconfig.uuidv4()}`,
              to: moveNode.id,
              parent: enteredEdge.parent
            });


            props.onChange({ nodes: [...upsetNodes], edges: [...upsetEdges], branchs: branchs });
          }
        }
        activeDrag = "";
        enteredEdge = null;
        moveNode = null
        setSelectMore(false)
        setDroppable(false);
      } else {
        //Drop Nodes
        if (droppable && (nodeIndex === 3 || nodeIndex === 9) && (enteredEdge)) {
          const id1 = `node-${nodeconfig.uuidv4()}`;
          const id2 = `node-${nodeconfig.uuidv4()}`;
          const id3 = `node-${nodeconfig.uuidv4()}`;
          const id4 = `node-${nodeconfig.uuidv4()}`;
          const id5 = `node-${nodeconfig.uuidv4()}`;

          let conditionNdes = [
            {
              id: id3,
              text: 'True',
              nodeType: 'conditionTrue',
              height: 25,
              width: 170,
            },
            {
              id: id2,
              text: 'False',
              nodeType: 'conditionFalse',
              height: 25,
              width: 170,
            },
          ]


          let conditionEdges = [
            {
              id: `node-if-up-${nodeconfig.uuidv4()}`,
              from: `${id1}`,
              to: `${id2}`,
              arrow: 'false',
              index: 1
            },
            {
              id: `node-if-up-${nodeconfig.uuidv4()}`,
              from: `${id1}`,
              to: `${id3}`,
              arrow: 'false',
              index: 2
            }
          ];

          //Branch condition
          if (nodeIndex === 9) {

            conditionNdes = conditionNdes.concat([
              {
                id: id4,
                text: 'False',
                nodeType: 'conditionFalse',
                height: 25,
                width: 170,
                index: 4
              },
              {
                id: id5,
                text: 'True',
                nodeType: 'conditionTrue',
                height: 25,
                width: 170,
                index: 3
              },
            ]);

            conditionNdes.forEach((ele, eleindex) => {
              ele.text = "Value-" + (eleindex + 1)
              ele.nodeType = 'conditionBranch'
            });

            conditionEdges = ([
              {
                id: `node-if-up-${nodeconfig.uuidv4()}`,
                from: `${id1}`,
                to: `${id4}`,
                arrow: 'false',
              },
              {
                id: `node-if-up-${nodeconfig.uuidv4()}`,
                from: `${id1}`,
                to: `${id5}`,
                arrow: 'false',
              }
            ]).concat(conditionEdges);
          }


          let condition = {
            id: id1,
            text: activeDrag,
            height: 39,
            width: 200,
            nodeType: activeDrag,
            data: {
              nodeType: activeDrag
            },
            icon: nodeIcon
          }

          //Branch condition
          if (nodeIndex === 9) {
            condition.text = activeDrag;
            condition.height = 39;
            condition.width = 200;
          }

          if (enteredEdge && enteredEdge.parent != "") {
            conditionNdes.forEach(ele => {
              ele["parent"] = enteredEdge.parent;
            });
            conditionEdges.forEach(ele => {
              ele["parent"] = enteredEdge.parent;
            });
            condition["parent"] = enteredEdge.parent;
          }

          const results = {
            nodes: [...nodes, ...conditionNdes, ...[condition]],
            edges: [...edges, ...conditionEdges],
          }

          results.edges.push({
            id: `node-${nodeconfig.uuidv4()}`,
            from: `${enteredEdge.from}`,
            to: `${condition.id}`,
            parent: enteredEdge.parent,
            arrow: 'false',
            index: 1
          },
          )

          let findIndex = results.edges.findIndex(x => (x.from == enteredEdge.from && x.to == enteredEdge.to));
          let arrayslice1 = results.edges.slice(0, findIndex + 1);
          let arrayslice2 = results.edges.slice(findIndex + 1, results.edges.length);
          arrayslice1[findIndex] = {
            id: `node-${nodeconfig.uuidv4()}`,
            from: `${conditionNdes[0].id}`,
            parent: conditionNdes[0].parent,
            to: `${enteredEdge.to}`,
          }

          conditionNdes.forEach((ele, index) => {
            if (index != 0) {
              arrayslice1.push({
                id: `node-${nodeconfig.uuidv4()}`,
                from: `${ele.id}`,
                parent: enteredEdge.parent,
                to: `${enteredEdge.to}`,
              })
            }
          })

          results.edges = arrayslice1.concat(arrayslice2)

          // Add Branch-endpoint 
          let tempBranch = branchs;
          tempBranch.push({
            "branch_id": id1,
            "stop_node_id": enteredEdge.to
          });

          props.onChange({ nodes: results.nodes, edges: results.edges, branchs: tempBranch });

        } else if (droppable && nodeIndex === 8 && (enteredEdge)) {
          const id1 = `while-node-${nodeconfig.uuidv4()}`;
          const id2 = `while-node-${nodeconfig.uuidv4()}`;
          const id3 = `while-node-${nodeconfig.uuidv4()}`;

          let whileNdes = [
            {
              id: id2,
              data: {
                nodeType: 'start'
              },
              height: 39,
              width: 200,
              parent: id1,
              nodeType: 'whileStart',
              icon: "play-circle"
            },
            {
              id: id3,
              data: {
                nodeType: 'stop'
              },
              nodeType: 'whileStop',
              height: 39,
              width: 200,
              parent: id1,
              icon: "stop-circle-o"
            },
          ]

          let whileEdges = [
            {
              id: `while-edges`,
              from: `${id2}`,
              to: `${id3}`,
              parent: id1,
            }
          ];

          let whileNodes = {
            id: id1,
            height: 80,
            title: "Loop",
            width: 75,
            nodeType: activeDrag,
            data: {
              nodeType: activeDrag
            },
          }

          if (enteredEdge && enteredEdge.parent != "") {
            whileNodes["parent"] = enteredEdge.parent;
          }


          const results = upsertNode([...nodes, ...whileNdes], [...edges, ...whileEdges], enteredEdge, whileNodes);

          if (results) {
            const singleNodes2 = stopArr.filter(function (o1) {
              return !results.edges.some(function (o2) {
                return o1.from == o2.from;
              });
            });


            props.onChange({ nodes: results.nodes, edges: results.edges, branchs: branchs });
            props.onDragEnd(whileNodes)
          }

        } else if (droppable && (enteredEdge)) {
          let newNode = {
            id,
            text: activeDrag,
            nodeType: activeDrag,
            data: {
              nodeType: activeDrag
            },
            height: 39,
            width: 200,
            icon: nodeIcon
          };

          if (enteredEdge && enteredEdge.parent != "") {
            newNode["parent"] = enteredEdge.parent;
            if (enteredEdge) {
              enteredEdge["parent"] = enteredEdge.parent;
            }

          }

          results = upsertNode(nodes, edges, enteredEdge, newNode);
          if (results) {
            const singleNodes2 = stopArr.filter(function (o1) {
              return !results.edges.some(function (o2) {
                return o1.from == o2.from;
              });
            });

            props.onChange({ nodes: results.nodes, edges: results.edges.concat(singleNodes2), branchs: branchs });
            props.onDragEnd(newNode)

          }
          perentChild = "";
        }

        activeDrag = "";
        enteredEdge = null;
        setSelectMore(false)
        setDroppable(false);
      }

    } catch (err) {
      console.log(err);
      activeDrag = "";
      enteredEdge = null;
      setSelectMore(false)
      setDroppable(false);
    }
  };

  const resetDragNode = () => {
    activeDrag = "";
    enteredEdge = null;
    moveNode = null
    setDroppable(false);
  }

  const edgeList = (edges) => {
    let uniqueChars = [];
    edges.forEach((c) => {
      if (!uniqueChars.includes(c)) {
        uniqueChars.push(c);
      }
    });
    return uniqueChars;
  }

  function setTimeoutaasunc() {
    return new Promise(function (resolve, reject) {
      setTimeout(function () {
        resolve("anything");
      }, 5000);
    });
  }



  const removeNodes = async (node) => {
    let stateNodes = nodes;
    let stateEdges = edges;
    if (node.data.nodeType == "Condition" || node.data.nodeType == "Branch Condition") {
      let branch = branchs.find(x => x.branch_id == node.id) ? branchs.find(x => x.branch_id == node.id) : null
      let edgeConditionTo = stateEdges.find(x => x.to == branch.stop_node_id);
      const getBranchdNodeEdge = getBranchdNodeEdgelinks(node, nodes, stateEdges, branch.stop_node_id);
      let upsetEdges = stateEdges.filter(x => (!getBranchdNodeEdge.edgesIds.includes(x.id)))
      let upsetbranch = branchs.filter(x => !(x.branch_id == node.id))

      // let upsetEdgesIndex = upsetEdges.findIndex(x => x.to == branch.branch_id);
      // if (upsetEdgesIndex >= 0) {
      //   upsetEdges[upsetEdgesIndex] = {
      //     from: upsetEdges[upsetEdgesIndex].from,
      //     id: upsetEdges[upsetEdgesIndex].id,
      //     to: branch.stop_node_id,
      //     parent: edgeConditionTo.parent
      //   }
      // }


      let upsetEdgesIndex = upsetEdges.findIndex(x => (x => x.to == branch.branch_id));
      let arrayslice1 = upsetEdges.slice(0, upsetEdgesIndex + 1);
      let arrayslice2 = upsetEdges.slice(upsetEdgesIndex + 1, upsetEdges.length);

      if (upsetEdgesIndex >= 0) {
        arrayslice1.push({
          from: stateEdges.find(x => x.to == branch.branch_id).from,
          id: upsetEdges[upsetEdgesIndex].id,
          to: branch.stop_node_id,
          parent: edgeConditionTo.parent
        })
      }

      upsetEdges = arrayslice1.concat(arrayslice2)
      upsetEdges = upsetEdges.filter(x => !(x.to == branch.branch_id));


      //Delete Connected Nodes
      let nodesIds = [];
      upsetEdges.forEach(ele => { nodesIds.push(ele.from); nodesIds.push(ele.to) })
      let upsetNodes = nodes.filter(x => ((nodesIds.includes(x.id))))
      setSelectMore(false);
      props.onChange({ nodes: upsetNodes, edges: upsetEdges, branchs: upsetbranch })
      clearSelections();


    } else {

      //New Delete Method
      let edgeLinkfilter = stateEdges.find(x => x.from == node.id)
      let edgeConditionFrom = stateEdges.find(x => x.to == node.id)
      let upsetEdges = [];
      let upsetNodes = stateNodes.filter(x => !(x.id == node.id));

      let isCheck = 0
      stateEdges.forEach(ele => {
        if (!(ele.id == edgeLinkfilter.id || ele.id == edgeConditionFrom.id)) {
          upsetEdges.push(ele)
        } else if ((ele.id == edgeLinkfilter.id) && (isCheck == 0)) {
          upsetEdges.push({
            from: edgeConditionFrom.from,
            id: `node-${nodeconfig.uuidv4()}`,
            to: edgeLinkfilter.to,
            parent: edgeLinkfilter.parent
          });
        }
      })

      setSelectMore(false)
      props.onChange({ nodes: upsetNodes, edges: upsetEdges, edges: upsetEdges, branchs: branchs })
      clearSelections();
    }
  }

  const getBranchdNodeEdgelinks = (node, nodes, stateEdges, stop_node_id) => {
    let edgesIds = [], nodesIds = [];
    const getEdges = (nodeId, initial) => {
      if (initial) {
        stateEdges.filter(x => ((x.from == nodeId)) && !(edgesIds.includes(x.id))).map(item => {
          edgesIds.push(item.id)
          nodesIds.push(item.to)
          if (stateEdges.find(x => (x.from == item.to))) {
            getEdges(item.to, true)
          }
        })
      } else if (nodeId != node.to) {
        edges.filter(x => ((x.from == nodeId) || (x.to == nodeId)) && !(edgesIds.includes(x.id))).map(item => {
          edgesIds.push(item.id)
          nodesIds.push(item.from)
          nodesIds.push(item.to)
          if (edges.find(x => x.from == (item.from) || (x.to == item.from))) {
            getEdges(item.from)
          }
          if (edges.find(x => (x.from == item.to && stop_node_id != item.to) || (x.to == item.to))) {
            getEdges(item.to)
          }
        })
      }

    }
    getEdges(node.id, true);
    return { edgesIds, nodesIds }
  }

  //deletedNodeEdge 1
  const deletedNodeEdge = (node, nodes, edges) => {
    let edgesIds = [], nodesIds = [];

    const getEdges = (nodeId) => {
      edges.filter(x => ((x.from == nodeId) || (x.to == nodeId)) && !(edgesIds.includes(x.id))).map(item => {
        edgesIds.push(item.id)
        nodesIds.push(item.from)
        nodesIds.push(item.to)
        if (edges.find(x => x.from == (item.from) || (x.to == item.from))) {
          getEdges(item.from)
        }
        if (edges.find(x => (x.from == item.to) || (x.to == item.to))) {
          getEdges(item.to)
        }
      })
    }
    getEdges(node.id);
    return { edgesIds, nodesIds }
  }

  //deletedNodeEdge 2
  const deletedNodeEdges = (argNodes, argEdges) => {
    let edgesIds = [];

    argNodes.forEach(ele => {
      let edgeData = argEdges.find(x => (x.from == ele.id || x.to == ele.id))
      if (edgeData) {
        edgesIds.push(edgeData.id)
      }
    })

    return { edgesIds }
  }

  const toggle = () => {
    setModalRename(!modal_rename)
  }

  const saveNameNode = () => {
    debugger
    let upsetNodes = nodes;
    let selectedIndex = upsetNodes.findIndex(x => x.id == selectNode.id)
    upsetNodes[selectedIndex].text = renameNode;
    setNodes([...upsetNodes])
    toggle();
    setRenameNode("")
    setSelectMore(false);
  }

  const onDisableNode = () => {
    let upsetNodes = nodes;
    let selectedIndex = upsetNodes.findIndex(x => x.id == selectNode.id)
    upsetNodes[selectedIndex]['selectionDisabled'] = selectNode.selectionDisabled === true ? false : true
    setNodes([...upsetNodes])
    props.onChange({ nodes: [...upsetNodes], edges: edges, branchs: branchs });
    setSelectMore(false);
  }

  return (
    <div className='glozic-flowdesign' style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }}>
      <ReactFlowLeft
        buttonRef={props.buttonRef}
        onDragStart={onDragStart}
        onDragEnd={onDragEnd}
        onCancel={props.onCancel}
        onSave={props.onSave}
        options={props.options} />
      <div className="react-flow-right">

        <pre style={{ zIndex: 9, position: 'absolute', bottom: 15, right: 15, background: 'rgba(0, 0, 0, .5)', padding: 20, color: 'white' }}>
          Zoom: {zoom}<br />
          <button style={{ display: 'block', width: '100%', margin: '5px 0' }} onClick={() => ref.current.zoomIn()}>Zoom In</button>
          <button style={{ display: 'block', width: '100%', margin: '5px 0' }} onClick={() => ref.current.zoomOut()}>Zoom Out</button>
          <button style={{ display: 'block', width: '100%' }} onClick={() => ref.current.fitCanvas()}>Fit</button>
        </pre>

        {selectMore &&
          <div
            onMouseLeave={(ey) => { setSelectMore(false) }}
            style={{ position: "fixed", top: nodeSelected.y, left: nodeSelected.x, width: '150px' }} class={`dropdown-menu-workfow-config`}>
            {/* <button class="d-flex dropdown-item" type="button" tabindex="0" data-test-id="action-menu-item" style={{ "word-break": "normal" }} onClick={() => { setSelectMore(false); }}>Copy</button> */}
            <button class="d-flex dropdown-item" type="button" tabindex="0" data-test-id="action-menu-item" style={{ "word-break": "normal" }} onClick={() => { setSelectMore(false); toggle(); setRenameNode(selectNode.text) }}>Rename</button>
            <button class="d-flex dropdown-item" type="button" tabindex="0" data-test-id="action-menu-item" style={{ "word-break": "normal" }} onClick={() => { setSelectMore(false); props.onNodeClick(selectNode) }}>Configure</button>
            <button class="d-flex dropdown-item" type="button" tabindex="0" data-test-id="action-menu-item" style={{ "word-break": "normal" }} onClick={() => { onDisableNode() }}>{selectNode.selectionDisabled === true ? 'Enable' : 'Disable'}</button>
            <button class="d-flex dropdown-item" type="button" tabindex="0" data-test-id="action-menu-item" style={{ "word-break": "normal" }} onClick={() => removeNodes(selectNode, false, false, false,)}>Delete</button>
          </div>
        }


        <Canvas
          ref={ref}
          zoomable={true}
          Height={10}
          className='Canvas'
          nodes={nodes}
          edges={edgeList(edges)}
          onEnter={(event, edge) => { console.log(edge); enteredEdge = edge }}
          // onLayoutChange={layout => console.log('Layout', layout)}
          layoutOptions={{ "elk.hierarchyHandling": "INCLUDE_CHILDREN" }}
          direction="DOWN"

          node={(event) => {

            let nodeclass = 'defaultNodeStyle', textFill = "#000";
            let masterNode = true;
            if (event.properties.data?.nodeType === 'start') {
              nodeclass = 'defaultNodeStyle';
              textFill = "#fff"
              masterNode = false
            } else if (event.properties.data?.nodeType === 'stop') {
              nodeclass = 'defaultNodeStyle';
              textFill = "#fff"
              masterNode = false
            } else if (selectNode && selectNode.id == event.properties.id) {
              // nodeclass = 'selectNodeStyle';
              // textFill = "#fff"
            }

            if (event.properties?.nodeType == "whileStart" || event.properties?.nodeType == "whileStop") {
              nodeclass += " whileNode"
              masterNode = false
            }

            if (event.properties.data?.nodeType != "Loop") {
              return (
                <Node
                  className='cursor-pointer'
                  style={{ fill: "#fff0", stroke: "#fff0" }}
                  label={""}>
                  {(event2, node) => {
                    return (
                      <foreignObject
                        onClick={() => { setSelectNode(event.properties); props.onNodeClick(event.properties) }}
                        height={event.properties.height}
                        width={event.properties.width}
                        x={0}
                        y={0}>

                        {(event.properties.data && ['start', 'stop'].includes(event.properties.nodeType)) &&
                          <div className={`${nodeclass} node-child noselect`} >
                            <NodeActionCard
                              masterNode={masterNode}
                              cardName={event.properties.text}
                              icon={event.properties.icon}
                              holderIndex={-1}
                              event={event}
                              props={{ setSelectNode, setNodeSelected, setSelectMore }}
                              droppable={droppable} />
                          </div>
                        }

                        {(['whileStart', 'whileStop'].includes(event.properties.nodeType)) &&
                          <div className={`${nodeclass} node-child noselect`} >
                            <NodeActionCard
                              masterNode={masterNode}
                              cardName={event.properties.text}
                              icon={event.properties.icon}
                              holderIndex={-1}
                              event={event}
                              props={{ setSelectNode, setNodeSelected, setSelectMore }}
                              droppable={droppable} />
                          </div>
                        }

                        {['conditionTrue', 'conditionFalse'].includes(event.properties.nodeType) &&
                          <div className={`node-child-lable noselect`}>
                            <label className='m-0' style={{ color: textFill }}>{event.properties.text}</label>
                          </div>
                        }

                        {(!nodeTypeCondition.includes(event.properties.nodeType)) &&
                          <motion.div
                            onMouseDown={mbevent => onMoveDragStart(mbevent, event.properties)}
                            className={`${event.properties.selectionDisabled === true && 'disabled-nodes'} postion-relative`} >
                            <div className={`${nodeclass} node-child noselect`} >
                              <NodeActionCard
                                masterNode={masterNode}
                                cardName={event.properties.text}
                                icon={event.properties.icon}
                                holderIndex={-1}
                                event={event}
                                props={{ setSelectNode, setNodeSelected, setSelectMore }}
                                droppable={droppable} />
                            </div>
                          </motion.div>
                        }

                      </foreignObject>
                    )
                  }}
                </Node>)
            } else {
              return (
                <Node className='cursor-pointer'>
                  {(event2, node) => {
                    return (
                      <foreignObject
                        className={`${event.properties.selectionDisabled === true && 'disabled-nodes'}`}
                        height={event2.height}
                        width={event2.width}
                        x={0}
                        y={0}
                        onClick={() => {
                          setSelectNode(event.properties);
                          props.onNodeClick(event.properties)
                        }}>
                        <motion.div className={`postion-relative`}>
                          <div className={`${nodeclass} node-child noselect`} onClick={onDragEnd}
                            onMouseDown={mbevent => onMoveDragStart(mbevent, event.properties)}>
                            <NodeActionCard
                              masterNode={masterNode}
                              event={event2}
                              cardName={event2.node.title}
                              icon={event.properties.icon}
                              holderIndex={-1}
                              even={event}
                              props={{ setSelectNode, setNodeSelected, setSelectMore }}
                              droppable={droppable} />
                          </div>
                        </motion.div>
                      </foreignObject>
                    )
                  }}
                </Node>
              )
            }
          }}

          edge={(eleEdge) => {
            const temp = {
              "width": "254px",
              "height": "40px",
              "x": eleEdge.sections[0].startPoint.x - 125,
              "y": eleEdge.sections[0].startPoint.y + 10,
              "color": "#fff",
              // "backgroundColor": "#2bbbad",
            }

            const temp2 = {
              "color": "#fff",
              "background": "#8080804a",
              "width": "254px",
              "height": "40px",
              "x": eleEdge.sections[0].startPoint.x - 125,
              "y": eleEdge.sections[0].startPoint.y + 10
            }

            return (
              <Edge
                style={{ markerEnd: eleEdge.id.search('node-if-up-') == -1 ? '' : 'inherit' }}
                add={
                  <foreignObject
                    hidden={(!(eleEdge.id.search('node-if-up-') == -1))}
                    x={eleEdge.sections[0].endPoint.x}
                    className="plus"
                    style={((!(eleEdge.id.search('node-if-up-') == -1) || !droppable || !enteredEdge || !(enteredEdge && enteredEdge.id == eleEdge.id))) ? temp : temp2}
                    onMouseEnter={(event, edges) => { enteredEdge = eleEdge.properties; setNodeSelected({ x: 264 + 20, y: 24 }) }}
                    onMouseLeave={(event, edges) => { enteredEdge = null; setNodeSelected({ x: 264 + 20, y: 24 }) }}
                    onClick={(ey) => {
                      setNodeSelected({ x: ey.clientX - 264 + 20, y: ey.clientY - 24 })
                      setSelectPast(!selectPast)
                    }}
                  />
                }

                onEnter={(event, edge) => { console.log(edge); }}
                onClick={(event, node) => {
                  console.log('Selecting Node', event, node);
                  console.log(selections);
                  onClick(event, node);
                }}
              />
            )
          }
          }

          onZoomChange={z => {
            console.log('zooming', z);
            setZoom(z);
          }}
        />

      </div>
      <div>
        <motion.div
          drag
          dragControls={dragControls}
          className="dragger"
          onDragEnd={onDragEnd}
        >
          {activeDrag && (
            <ActionCard cardName={activeDrag} icon={nodeIcon} holderIndex={-1} isDrage={activeDrag} />
          )}
        </motion.div>
      </div>

      <Modal isOpen={modal_rename} className='breakpoint-modal modal-dialog-centered' toggle={() => { toggle() }}>
        <div className="modal-header">
          <h5 className="modal-title mt-0" id="myModalLabel">Rename Node</h5>
          <button
            type="button"
            onClick={() => { { toggle() } }}
            className="close"
            data-dismiss="modal"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className='modal-body'>
          <div className="form">
            <div className='form-group row'>
              <label htmlFor="example-text-input" className="col-form-label col-md-2">Name</label>
              <input className="form-control col-md-9" type="input" name="firstName" value={renameNode} onChange={(e) => setRenameNode(e.target.value)} />
            </div>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" onClick={() => { toggle() }} className={`btn btn-secondary waves-effect`} data-dismiss="modal">Close</button>
          <button type="button" onClick={saveNameNode} className={`btn btn-primary waves-effect waves-light`}>Save changes</button>
        </div>
      </Modal >

    </div >
  );
}


export default FlowCompo;


