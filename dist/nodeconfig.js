"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es.regexp.exec.js");

require("core-js/modules/es.string.replace.js");

require("core-js/modules/es.regexp.to-string.js");

const uuidv4 = () => {
  return 'xxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0,
        v = c == 'x' ? r : r & 0x3 | 0x8;
    return v.toString(16);
  });
};

const startNodeId = "node-start";
const stopNodeId = "node-stop";
const defaultNodes = [{
  id: "".concat(startNodeId),
  text: 'Start',
  data: {
    nodeType: 'start'
  },
  height: 39,
  width: 200,
  nodeType: 'start',
  icon: "play-circle"
}, {
  id: "".concat(stopNodeId),
  text: 'Stop',
  data: {
    nodeType: 'stop'
  },
  height: 39,
  width: 200,
  nodeType: 'stop',
  icon: "stop-circle-o"
}];
const defaultEdge = [{
  id: "edge-".concat(uuidv4()),
  from: "".concat(startNodeId),
  to: "".concat(stopNodeId)
}];
var _default = {
  defaultNodes,
  defaultEdge,
  uuidv4
};
exports.default = _default;