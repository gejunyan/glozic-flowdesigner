"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.promise.js");

require("core-js/modules/es.string.includes.js");

require("core-js/modules/es.regexp.exec.js");

require("core-js/modules/es.string.search.js");

var _react = _interopRequireWildcard(require("react"));

var _reaflow = require("reaflow");

var _reactstrap = require("reactstrap");

var _framerMotion = require("framer-motion");

var _TaskPalette = _interopRequireDefault(require("./TaskPalette"));

var _nodeconfig = _interopRequireDefault(require("./nodeconfig"));

var _ActionCard = require("./ActionCard");

require("./index.css");

var _dagre = require("dagre");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

let enteredEdge = null;
let enteredNode = null;
let nodeIndex = "";
let activeDrag = "";
let perentChild = "";
let nodeIcon = "";
let moveNode = null;
let isNodeCheck = false;
const nodeTypeCondition = ['start', 'stop', 'conditionFalse', 'conditionTrue', 'conditionBranch', 'whileStart', 'whileStop'];

const FlowCompo = props => {
  const [droppable, setDroppable] = (0, _react.useState)(false);
  const [nodes, setNodes] = (0, _react.useState)(props.Nodes && props.Nodes.length != 0 ? props.Nodes : _nodeconfig.default.defaultNodes);
  const [edges, setEdges] = (0, _react.useState)(props.Edges && props.Edges.length != 0 ? props.Edges : _nodeconfig.default.defaultEdge);
  const [branchs, setBranchs] = (0, _react.useState)(props.branchs && props.branchs.length != 0 ? props.branchs : []);
  const [selectNode, setSelectNode] = (0, _react.useState)(false);
  const [selectMore, setSelectMore] = (0, _react.useState)(false);
  const [hiddenEdges, setHiddenEdges] = (0, _react.useState)([]);
  const [stopArr, setStopArr] = (0, _react.useState)([]);
  const [nodeSelected, setNodeSelected] = (0, _react.useState)([]);
  const dragControls = (0, _framerMotion.useDragControls)();
  const [zoom, setZoom] = (0, _react.useState)(0.7);
  const [modal_rename, setModalRename] = (0, _react.useState)(false);
  const [renameNode, setRenameNode] = (0, _react.useState)("");
  const [selectPast, setSelectPast] = (0, _react.useState)(false);
  const ref = (0, _react.useRef)(null);
  const {
    selections,
    onCanvasClick,
    onClick: _onClick,
    onKeyDown,
    clearSelections
  } = (0, _reaflow.useSelection)({
    nodes,
    edges,
    selections: [],
    onDataChange: (n, e) => {
      console.info('Data changed', n, e);
      setNodes(n);
      setEdges(e);
    },
    onSelection: s => {
      console.info('Selection', s);
      console.log(edges);
    }
  });
  (0, _react.useEffect)(() => {
    if (props.Nodes && props.Nodes.length != 0) {
      setNodes(props.Nodes);
    }

    if (props.Edges && props.Edges.length != 0) {
      setEdges(props.Edges);
    }
  }, [props]);

  const onDragStart = (event, title, index, nodeIcons) => {
    enteredEdge = "";
    activeDrag = title;
    nodeIndex = index;
    nodeIcon = nodeIcons;
    console.log(nodeIcon);
    setDroppable(true);
    dragControls.start(event, {
      snapToCursor: true
    });
  };

  const onMoveDragStart = async (event, node) => {
    console.log("============onMoveDragStart==============");
    setTimeout(() => {
      console.log("============onMoveDragStart setTimeout==============");

      if (!node.selectionDisabled && !isNodeCheck) {
        console.log('Start of Dragging', node);
        console.log('event', event);
        enteredEdge = "";
        activeDrag = !node.text || node.text == "" ? node.nodeType : node.text;
        nodeIndex = node.data ? node.data.nodeIndex : null;
        nodeIcon = node.icon;
        moveNode = node;
        setDroppable(true);
        setSelectMore(false);
        dragControls.start(event, {
          snapToCursor: true
        });
      }
    }, 150);
  };

  const onDragEnd = (event, data) => {
    console.log("=============onDragEnd=================");
    isNodeCheck = true;
    setTimeout(() => {
      isNodeCheck = false;
    }, 160);

    try {
      const id = "node-".concat(_nodeconfig.default.uuidv4());
      let results = null; //Move Nodes

      if (moveNode && enteredEdge) {
        if (!edges.some(x => x.from == enteredEdge.from && x.to == moveNode.id && moveNode.id == enteredEdge.to || x.from == moveNode.id && x.to == enteredEdge.to && moveNode.id == enteredEdge.from)) {
          console.log("moveNode", moveNode);
          console.log("enteredEdge", enteredEdge);
          console.log("edges", edges);

          if (moveNode.data.nodeType == "Condition" || moveNode.nodeType == "Branch Condition") {
            let branch = branchs.find(x => x.branch_id == moveNode.id) ? branchs.find(x => x.branch_id == moveNode.id) : null;
            let edgeLinkFrom = edges.find(x => x.to == branch.branch_id);
            let edgeConditionTo = edges.find(x => x.to == branch.stop_node_id);
            const getBranchdNodeEdge = getBranchdNodeEdgelinks(moveNode, nodes, edges, branch.stop_node_id);
            let upsetEdges = edges.filter(x => !(x.to == branch.stop_node_id && getBranchdNodeEdge.edgesIds.includes(x.id)) && !(x.to == branch.branch_id) && !(x.from == enteredEdge.from));
            let upsetBranchEdges = edges.filter(x => x.to == branch.stop_node_id || x.to == branch.branch_id);
            let nodesIds = [];
            let updateNodes = nodes;
            let upsetbranch = branchs;
            upsetEdges.push({
              from: edgeLinkFrom.from,
              id: "node-".concat(_nodeconfig.default.uuidv4()),
              to: edgeConditionTo.to,
              parent: edgeConditionTo.parent
            });
            upsetBranchEdges.forEach(ele => {
              if (ele.to == branch.branch_id) {
                ele.from = enteredEdge.from;
                ele["parent"] = enteredEdge.parent;
                upsetEdges.push(ele);
              }

              if (ele.to == branch.stop_node_id && enteredEdge.from != ele.from && getBranchdNodeEdge.edgesIds.includes(ele.id)) {
                ele.to = enteredEdge.to;
                ele["parent"] = enteredEdge.parent;
                upsetEdges.push(ele);
              }
            });
            upsetEdges.forEach(ele => {
              if (getBranchdNodeEdge.edgesIds.includes(ele.id)) {
                ele["parent"] = enteredEdge.parent;
                nodesIds.push(ele.from);
                nodesIds.push(ele.to);
              }
            });
            updateNodes.forEach(ele => {
              if (nodesIds.includes(ele.id)) {
                ele["parent"] = enteredEdge.parent;
              }
            });
            upsetbranch.forEach(ele => {
              if (ele.branch_id == moveNode.id) {
                ele.stop_node_id = enteredEdge.to;
              }
            });
            setSelectMore(false);
            props.onChange({
              nodes: updateNodes,
              edges: upsetEdges,
              branchs: upsetbranch
            });
            clearSelections();
          } else {
            let edgeLinkfilter = edges.find(x => x.from == moveNode.id);
            let edgeConditionFrom = edges.find(x => x.to == moveNode.id);
            let upsetEdges = [];
            let upsetNodes = nodes;
            let isCheck = 0;
            edges.forEach(ele => {
              if (!(ele.id == edgeLinkfilter.id || ele.id == edgeConditionFrom.id)) {
                upsetEdges.push(ele);
              } else if (ele.id == edgeLinkfilter.id && isCheck == 0) {
                upsetEdges.push({
                  from: edgeConditionFrom.from,
                  id: "node-".concat(_nodeconfig.default.uuidv4()),
                  to: edgeLinkfilter.to,
                  parent: ele.parent
                });
              }
            });
            upsetEdges.forEach(ele => {
              if (enteredEdge.to == ele.to && enteredEdge.from == ele.from) {
                ele.from = moveNode.id;
              }
            });
            upsetNodes.forEach(ele => {
              if (ele.id == moveNode.id) {
                ele.parent = enteredEdge.parent;
              }
            });
            upsetEdges.push({
              from: enteredEdge.from,
              id: "node-".concat(_nodeconfig.default.uuidv4()),
              to: moveNode.id,
              parent: enteredEdge.parent
            });
            props.onChange({
              nodes: [...upsetNodes],
              edges: [...upsetEdges],
              branchs: branchs
            });
          }
        }

        activeDrag = "";
        enteredEdge = null;
        moveNode = null;
        setSelectMore(false);
        setDroppable(false);
      } else {
        //Drop Nodes
        if (droppable && (nodeIndex === 3 || nodeIndex === 9) && enteredEdge) {
          const id1 = "node-".concat(_nodeconfig.default.uuidv4());
          const id2 = "node-".concat(_nodeconfig.default.uuidv4());
          const id3 = "node-".concat(_nodeconfig.default.uuidv4());
          const id4 = "node-".concat(_nodeconfig.default.uuidv4());
          const id5 = "node-".concat(_nodeconfig.default.uuidv4());
          let conditionNdes = [{
            id: id3,
            text: 'True',
            nodeType: 'conditionTrue',
            height: 25,
            width: 170
          }, {
            id: id2,
            text: 'False',
            nodeType: 'conditionFalse',
            height: 25,
            width: 170
          }];
          let conditionEdges = [{
            id: "node-if-up-".concat(_nodeconfig.default.uuidv4()),
            from: "".concat(id1),
            to: "".concat(id2),
            arrow: 'false',
            index: 1
          }, {
            id: "node-if-up-".concat(_nodeconfig.default.uuidv4()),
            from: "".concat(id1),
            to: "".concat(id3),
            arrow: 'false',
            index: 2
          }]; //Branch condition

          if (nodeIndex === 9) {
            conditionNdes = conditionNdes.concat([{
              id: id4,
              text: 'False',
              nodeType: 'conditionFalse',
              height: 25,
              width: 170,
              index: 4
            }, {
              id: id5,
              text: 'True',
              nodeType: 'conditionTrue',
              height: 25,
              width: 170,
              index: 3
            }]);
            conditionNdes.forEach((ele, eleindex) => {
              ele.text = "Value-" + (eleindex + 1);
              ele.nodeType = 'conditionBranch';
            });
            conditionEdges = [{
              id: "node-if-up-".concat(_nodeconfig.default.uuidv4()),
              from: "".concat(id1),
              to: "".concat(id4),
              arrow: 'false'
            }, {
              id: "node-if-up-".concat(_nodeconfig.default.uuidv4()),
              from: "".concat(id1),
              to: "".concat(id5),
              arrow: 'false'
            }].concat(conditionEdges);
          }

          let condition = {
            id: id1,
            text: activeDrag,
            height: 39,
            width: 200,
            nodeType: activeDrag,
            data: {
              nodeType: activeDrag
            },
            icon: nodeIcon
          }; //Branch condition

          if (nodeIndex === 9) {
            condition.text = activeDrag;
            condition.height = 39;
            condition.width = 200;
          }

          if (enteredEdge && enteredEdge.parent != "") {
            conditionNdes.forEach(ele => {
              ele["parent"] = enteredEdge.parent;
            });
            conditionEdges.forEach(ele => {
              ele["parent"] = enteredEdge.parent;
            });
            condition["parent"] = enteredEdge.parent;
          }

          const results = {
            nodes: [...nodes, ...conditionNdes, ...[condition]],
            edges: [...edges, ...conditionEdges]
          };
          results.edges.push({
            id: "node-".concat(_nodeconfig.default.uuidv4()),
            from: "".concat(enteredEdge.from),
            to: "".concat(condition.id),
            parent: enteredEdge.parent,
            arrow: 'false',
            index: 1
          });
          let findIndex = results.edges.findIndex(x => x.from == enteredEdge.from && x.to == enteredEdge.to);
          let arrayslice1 = results.edges.slice(0, findIndex + 1);
          let arrayslice2 = results.edges.slice(findIndex + 1, results.edges.length);
          arrayslice1[findIndex] = {
            id: "node-".concat(_nodeconfig.default.uuidv4()),
            from: "".concat(conditionNdes[0].id),
            parent: conditionNdes[0].parent,
            to: "".concat(enteredEdge.to)
          };
          conditionNdes.forEach((ele, index) => {
            if (index != 0) {
              arrayslice1.push({
                id: "node-".concat(_nodeconfig.default.uuidv4()),
                from: "".concat(ele.id),
                parent: enteredEdge.parent,
                to: "".concat(enteredEdge.to)
              });
            }
          });
          results.edges = arrayslice1.concat(arrayslice2); // Add Branch-endpoint 

          let tempBranch = branchs;
          tempBranch.push({
            "branch_id": id1,
            "stop_node_id": enteredEdge.to
          });
          props.onChange({
            nodes: results.nodes,
            edges: results.edges,
            branchs: tempBranch
          });
        } else if (droppable && nodeIndex === 8 && enteredEdge) {
          const id1 = "while-node-".concat(_nodeconfig.default.uuidv4());
          const id2 = "while-node-".concat(_nodeconfig.default.uuidv4());
          const id3 = "while-node-".concat(_nodeconfig.default.uuidv4());
          let whileNdes = [{
            id: id2,
            data: {
              nodeType: 'start'
            },
            height: 39,
            width: 200,
            parent: id1,
            nodeType: 'whileStart',
            icon: "play-circle"
          }, {
            id: id3,
            data: {
              nodeType: 'stop'
            },
            nodeType: 'whileStop',
            height: 39,
            width: 200,
            parent: id1,
            icon: "stop-circle-o"
          }];
          let whileEdges = [{
            id: "while-edges",
            from: "".concat(id2),
            to: "".concat(id3),
            parent: id1
          }];
          let whileNodes = {
            id: id1,
            height: 80,
            title: "Loop",
            width: 75,
            nodeType: activeDrag,
            data: {
              nodeType: activeDrag
            }
          };

          if (enteredEdge && enteredEdge.parent != "") {
            whileNodes["parent"] = enteredEdge.parent;
          }

          const results = (0, _reaflow.upsertNode)([...nodes, ...whileNdes], [...edges, ...whileEdges], enteredEdge, whileNodes);

          if (results) {
            const singleNodes2 = stopArr.filter(function (o1) {
              return !results.edges.some(function (o2) {
                return o1.from == o2.from;
              });
            });
            props.onChange({
              nodes: results.nodes,
              edges: results.edges,
              branchs: branchs
            });
            props.onDragEnd(whileNodes);
          }
        } else if (droppable && enteredEdge) {
          let newNode = {
            id,
            text: activeDrag,
            nodeType: activeDrag,
            data: {
              nodeType: activeDrag
            },
            height: 39,
            width: 200,
            icon: nodeIcon
          };

          if (enteredEdge && enteredEdge.parent != "") {
            newNode["parent"] = enteredEdge.parent;

            if (enteredEdge) {
              enteredEdge["parent"] = enteredEdge.parent;
            }
          }

          results = (0, _reaflow.upsertNode)(nodes, edges, enteredEdge, newNode);

          if (results) {
            const singleNodes2 = stopArr.filter(function (o1) {
              return !results.edges.some(function (o2) {
                return o1.from == o2.from;
              });
            });
            props.onChange({
              nodes: results.nodes,
              edges: results.edges.concat(singleNodes2),
              branchs: branchs
            });
            props.onDragEnd(newNode);
          }

          perentChild = "";
        }

        activeDrag = "";
        enteredEdge = null;
        setSelectMore(false);
        setDroppable(false);
      }
    } catch (err) {
      console.log(err);
      activeDrag = "";
      enteredEdge = null;
      setSelectMore(false);
      setDroppable(false);
    }
  };

  const resetDragNode = () => {
    activeDrag = "";
    enteredEdge = null;
    moveNode = null;
    setDroppable(false);
  };

  const edgeList = edges => {
    let uniqueChars = [];
    edges.forEach(c => {
      if (!uniqueChars.includes(c)) {
        uniqueChars.push(c);
      }
    });
    return uniqueChars;
  };

  function setTimeoutaasunc() {
    return new Promise(function (resolve, reject) {
      setTimeout(function () {
        resolve("anything");
      }, 5000);
    });
  }

  const removeNodes = async node => {
    console.log(node);
    let stateNodes = nodes;
    let stateEdges = edges;

    if (node.data.nodeType == "Condition" || node.data.nodeType == "Branch Condition") {
      let branch = branchs.find(x => x.branch_id == node.id) ? branchs.find(x => x.branch_id == node.id) : null;
      let edgeConditionTo = stateEdges.find(x => x.to == branch.stop_node_id);
      const getBranchdNodeEdge = getBranchdNodeEdgelinks(node, nodes, stateEdges, branch.stop_node_id);
      console.log(getBranchdNodeEdge);
      let upsetEdges = stateEdges.filter(x => !getBranchdNodeEdge.edgesIds.includes(x.id));
      let upsetbranch = branchs.filter(x => !(x.branch_id == node.id));
      let upsetEdgesIndex = upsetEdges.findIndex(x => x => x.to == branch.branch_id);
      let arrayslice1 = upsetEdges.slice(0, upsetEdgesIndex + 1);
      let arrayslice2 = upsetEdges.slice(upsetEdgesIndex + 1, upsetEdges.length);

      if (upsetEdgesIndex >= 0) {
        arrayslice1.push({
          from: stateEdges.find(x => x.to == branch.branch_id).from,
          id: "node-".concat(_nodeconfig.default.uuidv4()),
          to: branch.stop_node_id,
          parent: edgeConditionTo.parent
        });
      }

      upsetEdges = arrayslice1.concat(arrayslice2);
      upsetEdges = upsetEdges.filter(x => !(x.to == branch.branch_id)); //Delete Connected Nodes

      let nodesIds = [];
      upsetEdges.forEach(ele => {
        nodesIds.push(ele.from);
        nodesIds.push(ele.to);
      });
      let upsetNodes = nodes.filter(x => nodesIds.includes(x.id));
      setSelectMore(false);
      props.onChange({
        nodes: upsetNodes,
        edges: upsetEdges,
        branchs: upsetbranch
      });
      clearSelections();
    } else {
      //New Delete Method
      let edgeLinkfilter = stateEdges.find(x => x.from == node.id);
      let edgeConditionFrom = stateEdges.find(x => x.to == node.id);
      let upsetEdges = [];
      let upsetNodes = stateNodes.filter(x => !(x.id == node.id));
      let isCheck = 0;
      stateEdges.forEach(ele => {
        if (!(ele.id == edgeLinkfilter.id || ele.id == edgeConditionFrom.id)) {
          upsetEdges.push(ele);
        } else if (ele.id == edgeLinkfilter.id && isCheck == 0) {
          upsetEdges.push({
            from: edgeConditionFrom.from,
            id: "node-".concat(_nodeconfig.default.uuidv4()),
            to: edgeLinkfilter.to,
            parent: edgeLinkfilter.parent
          });
        }
      });
      setSelectMore(false);
      props.onChange({
        nodes: upsetNodes,
        edges: upsetEdges,
        edges: upsetEdges,
        branchs: branchs
      });
      clearSelections();
    }
  };

  const getBranchdNodeEdgelinks = (node, nodes, stateEdges, stop_node_id) => {
    let edgesIds = [],
        nodesIds = [];

    const getEdges = (nodeId, initial) => {
      if (initial) {
        stateEdges.filter(x => x.from == nodeId && !edgesIds.includes(x.id)).map(item => {
          edgesIds.push(item.id);
          nodesIds.push(item.to);

          if (stateEdges.find(x => x.from == item.to && x.from != stop_node_id)) {
            getEdges(item.to, true);
          }
        });
      } else if (nodeId != node.to) {
        edges.filter(x => (x.from == nodeId && x.from != stop_node_id || x.to == nodeId) && !edgesIds.includes(x.id)).map(item => {
          if (item.from != stop_node_id) {
            edgesIds.push(item.id);
            nodesIds.push(item.from);
            nodesIds.push(item.to);

            if (edges.find(x => x.from == item.from || x.to == item.from)) {
              getEdges(item.from);
            }

            if (edges.find(x => x.from == item.to && stop_node_id != item.to || x.to == item.to)) {
              getEdges(item.to);
            }
          }
        });
      }
    };

    getEdges(node.id, true);
    return {
      edgesIds,
      nodesIds
    };
  }; //deletedNodeEdge 1


  const deletedNodeEdge = (node, nodes, edges) => {
    let edgesIds = [],
        nodesIds = [];

    const getEdges = nodeId => {
      edges.filter(x => (x.from == nodeId || x.to == nodeId) && !edgesIds.includes(x.id)).map(item => {
        edgesIds.push(item.id);
        nodesIds.push(item.from);
        nodesIds.push(item.to);

        if (edges.find(x => x.from == item.from || x.to == item.from)) {
          getEdges(item.from);
        }

        if (edges.find(x => x.from == item.to || x.to == item.to)) {
          getEdges(item.to);
        }
      });
    };

    getEdges(node.id);
    return {
      edgesIds,
      nodesIds
    };
  }; //deletedNodeEdge 2


  const deletedNodeEdges = (argNodes, argEdges) => {
    let edgesIds = [];
    argNodes.forEach(ele => {
      let edgeData = argEdges.find(x => x.from == ele.id || x.to == ele.id);

      if (edgeData) {
        edgesIds.push(edgeData.id);
      }
    });
    return {
      edgesIds
    };
  };

  const _toggle = () => {
    setModalRename(!modal_rename);
  };

  const saveNameNode = () => {
    debugger;
    let upsetNodes = nodes;
    let selectedIndex = upsetNodes.findIndex(x => x.id == selectNode.id);
    upsetNodes[selectedIndex].text = renameNode;
    setNodes([...upsetNodes]);

    _toggle();

    setRenameNode("");
    setSelectMore(false);
  };

  const onDisableNode = () => {
    let upsetNodes = nodes;
    let selectedIndex = upsetNodes.findIndex(x => x.id == selectNode.id);
    upsetNodes[selectedIndex]['selectionDisabled'] = selectNode.selectionDisabled === true ? false : true;
    setNodes([...upsetNodes]);
    props.onChange({
      nodes: [...upsetNodes],
      edges: edges,
      branchs: branchs
    });
    setSelectMore(false);
  };

  return /*#__PURE__*/_react.default.createElement("div", {
    className: "glozic-flowdesign ".concat(props.isShow ? ' show' : ' hide'),
    style: {
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0
    }
  }, /*#__PURE__*/_react.default.createElement(_TaskPalette.default, {
    buttonRef: props.buttonRef,
    onDragStart: onDragStart,
    onDragEnd: onDragEnd,
    onCancel: props.onCancel,
    onSave: props.onSave,
    options: props.options
  }), props.isToggele && /*#__PURE__*/_react.default.createElement("div", {
    className: "sidebar-handle",
    onClick: props.handleToggle
  }, props.isShow ? /*#__PURE__*/_react.default.createElement("i", {
    className: "fa fa-angle-left fa-2x"
  }) : /*#__PURE__*/_react.default.createElement("i", {
    className: "fa fa-angle-right fa-2x"
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: "react-flow-right"
  }, /*#__PURE__*/_react.default.createElement("pre", {
    style: {
      zIndex: 9,
      position: 'absolute',
      bottom: 15,
      right: 15,
      background: 'rgba(0, 0, 0, .5)',
      padding: 20,
      color: 'white'
    }
  }, "Zoom: ", zoom, /*#__PURE__*/_react.default.createElement("br", null), /*#__PURE__*/_react.default.createElement("button", {
    style: {
      display: 'block',
      width: '100%',
      margin: '5px 0'
    },
    onClick: () => ref.current.zoomIn()
  }, "Zoom In"), /*#__PURE__*/_react.default.createElement("button", {
    style: {
      display: 'block',
      width: '100%',
      margin: '5px 0'
    },
    onClick: () => ref.current.zoomOut()
  }, "Zoom Out"), /*#__PURE__*/_react.default.createElement("button", {
    style: {
      display: 'block',
      width: '100%'
    },
    onClick: () => ref.current.fitCanvas()
  }, "Fit")), selectMore && /*#__PURE__*/_react.default.createElement("div", {
    onMouseLeave: ey => {
      setSelectMore(false);
    },
    style: {
      position: "fixed",
      top: nodeSelected.y,
      left: nodeSelected.x,
      width: '150px'
    },
    class: "dropdown-menu-workfow-config"
  }, /*#__PURE__*/_react.default.createElement("button", {
    class: "d-flex dropdown-item",
    type: "button",
    tabindex: "0",
    "data-test-id": "action-menu-item",
    style: {
      "word-break": "normal"
    },
    onClick: () => {
      setSelectMore(false);

      _toggle();

      setRenameNode(selectNode.text);
    }
  }, "Rename"), /*#__PURE__*/_react.default.createElement("button", {
    class: "d-flex dropdown-item",
    type: "button",
    tabindex: "0",
    "data-test-id": "action-menu-item",
    style: {
      "word-break": "normal"
    },
    onClick: () => {
      setSelectMore(false);
      props.onNodeClick(selectNode);
    }
  }, "Configure"), /*#__PURE__*/_react.default.createElement("button", {
    class: "d-flex dropdown-item",
    type: "button",
    tabindex: "0",
    "data-test-id": "action-menu-item",
    style: {
      "word-break": "normal"
    },
    onClick: () => {
      onDisableNode();
    }
  }, selectNode && selectNode.selectionDisabled === true ? 'Enable' : 'Disable'), /*#__PURE__*/_react.default.createElement("button", {
    class: "d-flex dropdown-item",
    type: "button",
    tabindex: "0",
    "data-test-id": "action-menu-item",
    style: {
      "word-break": "normal"
    },
    onClick: () => removeNodes(selectNode, false, false, false)
  }, "Delete")), /*#__PURE__*/_react.default.createElement(_reaflow.Canvas, {
    ref: ref,
    zoomable: true,
    Height: 10,
    className: "Canvas",
    nodes: nodes,
    edges: edgeList(edges),
    onEnter: (event, edge) => {
      console.log(edge);
      enteredEdge = edge;
    },
    layoutOptions: {
      "elk.hierarchyHandling": "INCLUDE_CHILDREN"
    },
    direction: "DOWN",
    node: event => {
      var _event$properties$dat, _event$properties$dat2, _event$properties, _event$properties2, _event$properties$dat3;

      let nodeclass = 'defaultNodeStyle',
          textFill = "#000";
      let masterNode = true;

      if (((_event$properties$dat = event.properties.data) === null || _event$properties$dat === void 0 ? void 0 : _event$properties$dat.nodeType) === 'start') {
        nodeclass = 'defaultNodeStyle';
        textFill = "#fff";
        masterNode = false;
      } else if (((_event$properties$dat2 = event.properties.data) === null || _event$properties$dat2 === void 0 ? void 0 : _event$properties$dat2.nodeType) === 'stop') {
        nodeclass = 'defaultNodeStyle';
        textFill = "#fff";
        masterNode = false;
      } else if (selectNode && selectNode.id == event.properties.id) {// nodeclass = 'selectNodeStyle';
        // textFill = "#fff"
      }

      if (((_event$properties = event.properties) === null || _event$properties === void 0 ? void 0 : _event$properties.nodeType) == "whileStart" || ((_event$properties2 = event.properties) === null || _event$properties2 === void 0 ? void 0 : _event$properties2.nodeType) == "whileStop") {
        nodeclass += " whileNode";
        masterNode = false;
      }

      if (((_event$properties$dat3 = event.properties.data) === null || _event$properties$dat3 === void 0 ? void 0 : _event$properties$dat3.nodeType) != "Loop") {
        return /*#__PURE__*/_react.default.createElement(_reaflow.Node, {
          className: "cursor-pointer",
          style: {
            fill: "#fff0",
            stroke: "#fff0"
          },
          label: ""
        }, (event2, node) => {
          return /*#__PURE__*/_react.default.createElement("foreignObject", {
            onClick: () => {
              setSelectNode(event.properties);
              props.onNodeClick(event.properties);
              isNodeCheck = true;
            },
            height: event.properties.height,
            width: event.properties.width,
            x: 0,
            y: 0
          }, event.properties.data && ['start', 'stop'].includes(event.properties.nodeType) && /*#__PURE__*/_react.default.createElement("div", {
            className: "".concat(nodeclass, " node-child noselect")
          }, /*#__PURE__*/_react.default.createElement(_ActionCard.NodeActionCard, {
            masterNode: masterNode,
            cardName: event.properties.text,
            icon: event.properties.icon,
            holderIndex: -1,
            event: event,
            props: {
              setSelectNode,
              setNodeSelected,
              setSelectMore
            },
            droppable: droppable
          })), ['whileStart', 'whileStop'].includes(event.properties.nodeType) && /*#__PURE__*/_react.default.createElement("div", {
            className: "".concat(nodeclass, " node-child noselect")
          }, /*#__PURE__*/_react.default.createElement(_ActionCard.NodeActionCard, {
            masterNode: masterNode,
            cardName: event.properties.text,
            icon: event.properties.icon,
            holderIndex: -1,
            event: event,
            props: {
              setSelectNode,
              setNodeSelected,
              setSelectMore
            },
            droppable: droppable
          })), ['conditionTrue', 'conditionFalse'].includes(event.properties.nodeType) && /*#__PURE__*/_react.default.createElement("div", {
            className: "node-child-lable noselect"
          }, /*#__PURE__*/_react.default.createElement("label", {
            className: "m-0",
            style: {
              color: textFill
            }
          }, event.properties.text)), !nodeTypeCondition.includes(event.properties.nodeType) && /*#__PURE__*/_react.default.createElement(_framerMotion.motion.div, {
            onClick: onDragEnd,
            onMouseDown: mbevent => onMoveDragStart(mbevent, event.properties),
            className: "".concat(event.properties.selectionDisabled === true && 'disabled-nodes', " postion-relative")
          }, /*#__PURE__*/_react.default.createElement("div", {
            className: "".concat(nodeclass, " node-child noselect")
          }, /*#__PURE__*/_react.default.createElement(_ActionCard.NodeActionCard, {
            masterNode: masterNode,
            cardName: event.properties.text,
            icon: event.properties.icon,
            holderIndex: -1,
            event: event,
            props: {
              setSelectNode,
              setNodeSelected,
              setSelectMore
            },
            droppable: droppable
          }))));
        });
      } else {
        return /*#__PURE__*/_react.default.createElement(_reaflow.Node, {
          className: "cursor-pointer"
        }, (event2, node) => {
          return /*#__PURE__*/_react.default.createElement("foreignObject", {
            className: "".concat(event.properties.selectionDisabled === true && 'disabled-nodes'),
            height: event2.height,
            width: event2.width,
            x: 0,
            y: 0,
            onClick: () => {
              console.log(event);
              setSelectNode(event.properties);
              props.onNodeClick(event.properties);
            }
          }, /*#__PURE__*/_react.default.createElement(_framerMotion.motion.div, {
            className: "postion-relative"
          }, /*#__PURE__*/_react.default.createElement("div", {
            className: "".concat(nodeclass, " node-child noselect"),
            onClick: onDragEnd,
            onMouseDown: mbevent => onMoveDragStart(mbevent, event.properties)
          }, /*#__PURE__*/_react.default.createElement(_ActionCard.NodeActionCard, {
            masterNode: masterNode,
            cardName: event2.node.title,
            icon: event.properties.icon,
            holderIndex: -1,
            event: event,
            props: {
              setSelectNode,
              setNodeSelected,
              setSelectMore
            },
            droppable: droppable
          }))));
        });
      }
    },
    edge: eleEdge => {
      const temp = {
        "width": "254px",
        "height": "40px",
        "x": eleEdge.sections[0].startPoint.x - 125,
        "y": eleEdge.sections[0].startPoint.y + 10,
        "color": "#fff" // "backgroundColor": "#2bbbad",

      };
      const temp2 = {
        "color": "#fff",
        "background": "#8080804a",
        "width": "254px",
        "height": "40px",
        "x": eleEdge.sections[0].startPoint.x - 125,
        "y": eleEdge.sections[0].startPoint.y + 10
      };
      return /*#__PURE__*/_react.default.createElement(_reaflow.Edge, {
        style: {
          markerEnd: eleEdge.id.search('node-if-up-') == -1 ? '' : 'inherit'
        },
        add: /*#__PURE__*/_react.default.createElement("foreignObject", {
          hidden: !(eleEdge.id.search('node-if-up-') == -1),
          x: eleEdge.sections[0].endPoint.x,
          className: "plus",
          style: !(eleEdge.id.search('node-if-up-') == -1) || !droppable || !enteredEdge || !(enteredEdge && enteredEdge.id == eleEdge.id) ? temp : temp2,
          onMouseEnter: (event, edges) => {
            enteredEdge = eleEdge.properties;
            setNodeSelected({
              x: 264 + 20,
              y: 24
            });
          },
          onMouseLeave: (event, edges) => {
            enteredEdge = null;
            setNodeSelected({
              x: 264 + 20,
              y: 24
            });
          },
          onClick: ey => {
            setNodeSelected({
              x: ey.clientX - 264 + 20,
              y: ey.clientY - 24
            });
            setSelectPast(!selectPast);
          }
        }),
        onEnter: (event, edge) => {
          console.log(edge);
        },
        onClick: (event, node) => {
          console.log('Selecting Node', event, node);
          console.log(selections);

          _onClick(event, node);
        }
      });
    },
    onZoomChange: z => {
      console.log('zooming', z);
      setZoom(z);
    }
  })), /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(_framerMotion.motion.div, {
    drag: true,
    dragControls: dragControls,
    className: "dragger",
    onDragEnd: onDragEnd
  }, activeDrag && /*#__PURE__*/_react.default.createElement(_ActionCard.ActionCard, {
    cardName: activeDrag,
    icon: nodeIcon,
    holderIndex: -1,
    isDrage: activeDrag
  }))), /*#__PURE__*/_react.default.createElement(_reactstrap.Modal, {
    isOpen: modal_rename,
    className: "breakpoint-modal modal-dialog-centered",
    toggle: () => {
      _toggle();
    }
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "modal-header"
  }, /*#__PURE__*/_react.default.createElement("h5", {
    className: "modal-title mt-0",
    id: "myModalLabel"
  }, "Rename Node"), /*#__PURE__*/_react.default.createElement("button", {
    type: "button",
    onClick: () => {
      {
        _toggle();
      }
    },
    className: "close",
    "data-dismiss": "modal",
    "aria-label": "Close"
  }, /*#__PURE__*/_react.default.createElement("span", {
    "aria-hidden": "true"
  }, "\xD7"))), /*#__PURE__*/_react.default.createElement("div", {
    className: "modal-body"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "form"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "form-group row"
  }, /*#__PURE__*/_react.default.createElement("label", {
    htmlFor: "example-text-input",
    className: "col-form-label col-md-2"
  }, "Name"), /*#__PURE__*/_react.default.createElement("input", {
    className: "form-control col-md-9",
    type: "input",
    name: "firstName",
    value: renameNode,
    onChange: e => setRenameNode(e.target.value)
  })))), /*#__PURE__*/_react.default.createElement("div", {
    className: "modal-footer"
  }, /*#__PURE__*/_react.default.createElement("button", {
    type: "button",
    onClick: () => {
      _toggle();
    },
    className: "btn btn-secondary waves-effect",
    "data-dismiss": "modal"
  }, "Close"), /*#__PURE__*/_react.default.createElement("button", {
    type: "button",
    onClick: saveNameNode,
    className: "btn btn-primary waves-effect waves-light"
  }, "Save changes"))));
};

var _default = FlowCompo;
exports.default = _default;