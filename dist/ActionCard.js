"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ActionCard = ActionCard;
exports.NodeActionCard = NodeActionCard;

require("core-js/modules/es.json.stringify.js");

var _react = _interopRequireDefault(require("react"));

require("./ActionCard.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const style = {
  border: 'rounded 1px dashed gray',
  marginBottom: '0.2rem',
  backgroundColor: 'white',
  cursor: 'pointer',
  float: 'left',
  width: '100%',
  maxWidth: '200px',
  minWidth: '200px',
  verticalAlign: 'middle'
};
const nodeStyle = {
  border: 'rounded 1px dashed gray',
  marginBottom: '0.2rem',
  backgroundColor: 'white',
  cursor: 'pointer',
  float: 'left',
  width: '100%',
  verticalAlign: 'middle'
};
const style1 = {
  border: '1px dashed #2bbbad',
  backgroundColor: '#2bbbad',
  color: "#fff",
  cursor: 'pointer',
  float: 'left',
  width: '37px',
  margin: '0 auto',
  height: '38px'
};
const style2 = {
  backgroundColor: '#fff  ',
  cursor: 'pointer',
  float: 'left',
  width: '100%',
  height: '37px',
  verticalAlign: 'middle',
  color: "#000"
};

function ActionCard(_ref) {
  let {
    icon,
    cardName,
    holderIndex,
    isDrage
  } = _ref;
  let actionCardStyle = JSON.parse(JSON.stringify(style));
  let actionCardStyle1 = JSON.parse(JSON.stringify(style1));
  let actionCardStyle2 = JSON.parse(JSON.stringify(style2));
  actionCardStyle.backgroundColor = isDrage ? "rounded 1px dashed gray" : style.backgroundColor;
  actionCardStyle1.backgroundColor = isDrage ? "rgb(43 187 173 / 54%)" : style1.backgroundColor;
  actionCardStyle2.backgroundColor = isDrage ? "rgb(255 255 255 / 25%)" : style2.backgroundColor;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "d-flex justify-content-start .z-depth-1-half",
    style: _objectSpread({}, actionCardStyle)
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "p-2 col-example text-left",
    style: _objectSpread({}, actionCardStyle1)
  }, /*#__PURE__*/_react.default.createElement("i", {
    className: "fa fa-" + icon,
    "aria-hidden": "true"
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: "py-2 pl-1 col-example text-left",
    style: _objectSpread({}, actionCardStyle2)
  }, cardName));
}

function NodeActionCard(_ref2) {
  let {
    icon,
    cardName,
    holderIndex,
    event,
    props,
    droppable,
    masterNode,
    onClick,
    onMouseDown
  } = _ref2;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "d-flex justify-content-start .z-depth-1-half",
    style: _objectSpread({}, nodeStyle)
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "p-2 col-example text-left",
    onClick: onClick,
    onMouseDown: onMouseDown,
    style: _objectSpread({}, style1)
  }, /*#__PURE__*/_react.default.createElement("i", {
    className: "fa fa-" + icon,
    "aria-hidden": "true"
  })), /*#__PURE__*/_react.default.createElement("div", {
    className: "py-2 pl-1 col-example text-left",
    style: _objectSpread({}, style2)
  }, cardName), masterNode && /*#__PURE__*/_react.default.createElement("i", {
    className: "fa fa-cog position-absolute",
    style: {
      top: "10px",
      right: "9px"
    },
    "aria-hidden": "true",
    onMouseOver: ey => {
      if (!droppable) {
        props.setSelectNode(event.properties);
        props.setNodeSelected({
          x: ey.clientX - 2,
          y: ey.clientY - 10
        });
        props.setSelectMore(true);
      }
    }
  }));
}